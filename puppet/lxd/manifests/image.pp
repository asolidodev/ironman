## Manage lxd images
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.
# Copyright 2020 The LXD Puppet module Authors. All rights reserved.

define lxd::image(
    $repo_url    = undef,
    $image_file  = undef,
    $image_alias = undef,
    $username    = undef,
    $password    = undef,
    $image_name  = undef,
    $ensure      = 'present',
) { 
    Exec { path => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/', '/snap/bin/'] }

    case $ensure {
        # downloads the image from LXD's official server
        'official': {
            if $image_name != undef {
                exec { "lxd official image present ${image_name} container" :
                    command => "lxc image copy ${image_name} local: --alias ${image_alias}",
                    unless  => "lxc image ls -cl --format csv | grep '^${image_alias}$'",
                    timeout => 600,
                }
            } else {
                fail("Official image_name not provided. (e.g.: ubuntu:20.04)")
            }
        }

        'official-vm': {
            if $image_name != undef {
                exec { "lxd official image present ${image_name} vm" :
                    command => "lxc image copy ${image_name} local: --alias ${image_alias} --vm",
                    unless  => "lxc image ls -cl --format csv | grep '^${image_alias}$'",
                    timeout => 600,
                }
            } else {
                fail("Official image_name not provided. (e.g.: ubuntu:20.04)")
            }
        }

        'present': {
            validate_re($repo_url, "[^;']+")
            validate_re($image_file, "[^;']+")
            validate_re($image_alias, "[^;']+")

            if (username != undef) and ($password != undef) {
                exec { "lxd image present ${repo_url}/${image_file}":
                    command => "rm -f /tmp/puppet-download-lxd-image && wget --user '${username}' --password '${password}' -qO - '${repo_url}/${image_file}' > /tmp/puppet-download-lxd-image && lxc image import /tmp/puppet-download-lxd-image --alias '${image_alias}' && rm -f /tmp/puppet-download-lxd-image",
                    unless  => "lxc image ls -cl --format csv | grep '^${image_alias}$'",
                    timeout => 3600,
                }
            } else {
                exec { "lxd image present ${repo_url}/${image_file}":
                    command => "rm -f /tmp/puppet-download-lxd-image && wget -qO - '${repo_url}/${image_file}' > /tmp/puppet-download-lxd-image && lxc image import /tmp/puppet-download-lxd-image --alias '${image_alias}' && rm -f /tmp/puppet-download-lxd-image",
                    unless  => "lxc image ls -cl --format csv | grep '^${image_alias}$'",
                    timeout => 600,
                }
            }
        }

        'absent': {
            exec { "lxd image absent ${repo_url}/${image_file}":
                command => "lxc image rm '${image_alias}'",
                onlyif  => "lxc image ls -cl --format csv | grep '^${image_alias}$'",
                timeout => 600,
            }
        }

        default: {
            fail("Wrong ensure value: ${ensure}")
        }
    }
}
