## LXD container define
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.
# Copyright 2020 The LXD Puppet module Authors. All rights reserved.

define lxd::container(
    $image,
    $type,
    Boolean $set_ip,
    Boolean $set_private_ip,
    $config             = {},
    $devices            = {},
    $profiles           = ['default'],
    $state              = 'started',
    $ensure             = 'present',
    $ip_address         = undef,
    $netmask            = undef,
    $gateway            = undef,
    $dns                = [],
    $private_ip_address = undef,
    $private_netmask    = undef,
    $private_gateway    = undef,
    $target             = undef,
    $private_dns        = [],
) {

    # check if instance is in clustered environment
    if $target {
        # move the instance in case of error state
        exec { "move instance ${name} in case of error" :
            command => "lxc mv ${name} --target ${target}",
            path    => '/snap/bin:/usr/bin',
            onlyif  => "lxc cluster list && lxc list ^${name}$ | grep -w 'ERROR'",
        }

        # creating lxd container
        lxd_container { $name:
            ensure   => $ensure,
            state    => $state,
            config   => $config,
            devices  => $devices,
            profiles => $profiles,
            image    => $image,
            type     => $type,
            target   => $target,
            require  => Exec["move instance ${name} in case of error"],
        }
    
        -> lxd::network { "set ${name} ip":
            instance_name      => $name,
            state              => $state,
            set_ip             => $set_ip,
            ip_address         => $ip_address,
            netmask            => $netmask,
            gateway            => $gateway,
            dns                => $dns,
            set_private_ip     => $set_private_ip,
            private_ip_address => $private_ip_address,
            private_netmask    => $private_netmask,
            private_gateway    => $private_gateway,
            private_dns        => $private_dns,
        }
    
        exec { "move instance ${name} to ${target}":
            command => "lxc mv ${name} --target ${target}",
            path    => '/snap/bin:/usr/bin',
            onlyif  => 'lxc cluster list',
            unless  => "lxc info ${name} | grep \"Location: ${target}\"",
            require => Lxd::Network["set ${name} ip"],
        }
    } else {

        # creating lxd container
        lxd_container { $name:
            ensure   => $ensure,
            state    => $state,
            config   => $config,
            devices  => $devices,
            profiles => $profiles,
            image    => $image,
            type     => $type,
            target   => $target,
        }
   
        -> lxd::network { "set ${name} ip":
            instance_name      => $name,
            state              => $state,
            set_ip             => $set_ip,
            ip_address         => $ip_address,
            netmask            => $netmask,
            gateway            => $gateway,
            dns                => $dns,
            set_private_ip     => $set_private_ip,
            private_ip_address => $private_ip_address,
            private_netmask    => $private_netmask,
            private_gateway    => $private_gateway,
            private_dns        => $private_dns,
        }
    }

    case $ensure {
        'present': {
            Lxd::Image[$image]
            -> Lxd::Container[$name]
        }
        'absent': {
            Lxd::Container[$name]
            -> Lxd::Image[$image]
        }
        default : {
            fail("Unsuported ensure value ${ensure}")
        }
    }
}
