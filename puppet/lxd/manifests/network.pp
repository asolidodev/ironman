define lxd::network (
    Boolean $set_ip,
    Boolean $set_private_ip,
    $instance_name,
    $state,
    $ip_address         = undef,
    $netmask            = undef,
    $gateway            = undef,
    $dns                = [],
    $private_ip_address = undef,
    $private_netmask    = undef,
    $private_gateway    = undef,
    $private_dns        = [],
){
    if $state == 'started' {
      $localdir = lookup('filesystem::localdir')

      if $set_ip and $set_private_ip {
        if $ip_address == undef {
          fail("No IP address given.")
        } elsif $netmask == undef {
          fail("No netmask given.")
        } elsif $gateway == undef {
          fail("No gateway address given.")
        } elsif length($dns) == 0 {
          fail("No default DNS server given.")
        } elsif $private_ip_address == undef {
          fail("No private IP address given.")
        } elsif $private_netmask == undef {
          fail("No private netmask given.")
        } elsif $private_gateway == undef {
          fail("No private gateway given.")
        }

        if length($dns) > 3 {
          notify{"warning":
            message => "Warning: Maximum number of DNS servers accepted is 3.",
          }
        }

        $parsed_dns = join($dns,' ')
        $parsed_private_dns = join($private_dns,' ')

        exec { "$instance_name lxd network":
          command => "$localdir/bin/lxd-network.sh ${instance_name} ${ip_address} ${netmask} ${gateway} ${parsed_dns}",
        }

        exec { "$instance_name lxd private network":
          command => "$localdir/bin/lxd-private-network.sh ${instance_name} ${private_ip_address} ${private_netmask} ${private_gateway} ${parsed_private_dns}",
          require => Exec["$instance_name lxd network"],
        }

      } elsif $set_ip {
        if $ip_address == undef {
          fail("No IP address given.")
        } elsif $netmask == undef {
          fail("No netmask given.")
        } elsif $gateway == undef {
          fail("No gateway address given.")
        } elsif length($dns) == 0 {
          fail("No default DNS server given.")
        }

        if length($dns) > 3 {
          notify{"warning":
            message => "Warning: Maximum number of DNS servers accepted is 3.",
          }
        }

        $parsed_dns = join($dns,' ')

        exec { "$instance_name lxd network":
          command => "$localdir/bin/lxd-network.sh ${instance_name} ${ip_address} ${netmask} ${gateway} ${parsed_dns}",
        }
      } elsif $set_private_ip {
        if $private_ip_address == undef {
          fail("No private IP address given.")
        } elsif $private_netmask == undef {
          fail("No netmask given.")
        } elsif $private_gateway == undef {
          fail("No gateway given.")
        }

        $parsed_private_dns = join($private_dns,' ')

        exec { "$instance_name lxd private network":
          command => "$localdir/bin/lxd-private-network.sh ${instance_name} ${private_ip_address} ${private_netmask} ${private_gateway} ${parsed_private_dns}",
        }
      } else {
        # do nothing
      }
    } else {
      # do nothing
    }
}
