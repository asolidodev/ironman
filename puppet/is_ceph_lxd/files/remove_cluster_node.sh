#!/bin/bash
NODE_NAME=$1

if [ -z "$NODE_NAME" ]; then
    echo "The script should be used as follows:
    ./remove_cluster_node.sh NODE_HOSTNAME"
    exit 1
fi

lxc cluster list | grep -w $NODE_NAME &> /dev/null 
if [ $? -ne 0 ]; then
    echo "ERROR: Node '$NODE_NAME' does not exist."
    exit 1
fi

echo -n "Are you sure you want to remove node '$NODE_NAME' from the cluster (yes/no)?: "
while true
do
    read ANSWER
    if [ "$ANSWER" == 'yes' ]; then
        break
    elif [ "$ANSWER" == 'no' ]; then
        exit 0
    else
        echo -n "Please enter 'yes' or 'no': "
    fi
done

lxc cluster remove $NODE_NAME --force

OSDS=$(ceph osd df plain $NODE_NAME | head -n -2 | tail -n +2)
while IFS= read -r line; do
    OSD_ID=$(echo $line | grep -o -E '[0-9]+' | head -1)
    if [ ! -z $OSD_ID ]; then
        ceph osd down osd.$OSD_ID
        ceph osd purge osd.$OSD_ID --yes-i-really-mean-it
    fi
done <<< $OSDS

ceph orch host rm $NODE_NAME
ceph mon remove $NODE_NAME &> /dev/null

echo "Node '$NODE_NAME' removed."
