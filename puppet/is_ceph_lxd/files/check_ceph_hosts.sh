ARGS=$@
RESULT=0
for host in "$@" ; do
    if ! ceph orch host ls | grep $host > /dev/null; then
        RESULT=1
    fi
done

if [ $RESULT -eq 0 ]; then
    exit 0
else
    exit 1
fi
