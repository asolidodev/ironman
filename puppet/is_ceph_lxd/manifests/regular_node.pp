class is_ceph_lxd::regular_node(
  $public_network_gateway,
  $public_network_address,
  $public_network_interface,
  $backside_network_bridge_interface,
  $private_network_interface_address,
  $lxd_version,
  $ceph_pubkey,
  $bootstrap_ip,
  $secret_passwd,
  $nodes,
  $public_network_bridge_interface = 'br0',
  $private_network_interface       = 'lxdbr0',
  $dns_servers                     = ['8.8.8.8','1.1.1.1'],
){

  $hostname     = $facts['networking']['hostname']
  $ip           = $nodes[$hostname]['public_network_address']
  $ceph_lxd_ip  = $nodes[$hostname]['backside_network_address']
  $preseed_path = '/root/host_preseed.yaml'
  $localdir     = lookup('filesystem::localdir')

  file { "${localdir}/bin/remove_cluster_node.sh" :
    source => 'puppet:///modules/is_ceph_lxd/remove_cluster_node.sh',
    owner  => 'root',
    group  => 'root',
    mode   => '0744',
  }  

  class { 'is_ceph_lxd::dependencies': }
  class { 'is_lxd::lxd_server_base' :
    lxd_version                        => $lxd_version,
    lan_address                        => $public_network_address,
    lan_network_interface              => $public_network_bridge_interface,
    private_network_interface          => $private_network_interface,
    private_address                    => $private_network_interface_address,
    backside_network_interface         => $nodes[$hostname]['backside_network_interface'],
    backside_network_bridge_interface  => $backside_network_bridge_interface,
    backside_network_address           => $nodes[$hostname]['backside_network_address'],
    gw4                                => $public_network_gateway,
    dns_servers                        => $dns_servers,
    primary_interface                  => $public_network_interface,
    ip                                 => $ip,
    clustering                         => true,
  }

  # Allow root SSH access with the Ceph generated Key
  ssh_authorized_key { 'ceph':
    ensure => present,
    user   => 'root',
    type   => ssh-rsa,
    key    => $ceph_pubkey,
  }

  exec { "add host to LXD cluster":
    command => "write_host_preseed.sh ${bootstrap_ip} ${ceph_lxd_ip} ${secret_passwd} ${preseed_path}",
    path    => "${localdir}/bin:/usr/bin:/snap/bin",
    onlyif  => "test -e ${localdir}/bin/write_host_preseed.sh",
    unless  => "lxc cluster list | grep ${hostname}",
  }
}
