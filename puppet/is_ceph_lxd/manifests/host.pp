# This defined type configures a Ceph host on the boostrap node
define is_ceph_lxd::host(
  $hostname,
  $ip,
  Array[String] $devices = [],
){

  # must be the same as the path in is_ceph_lxd::bootstrap
  $key_pair_path = '/etc/ceph/ceph'
  $ssh_conf_file = '/etc/ssh/ssh_config.d/node_access.conf'
  $localdir      = lookup('filesystem::localdir')

  # Setup SSH access to the target host
  host { "host ${hostname}":
    ensure => present,
    name   => $hostname,
    ip     => $ip,
  }

  ssh_config { "HostName ${hostname}":
    ensure => present,
    host   => $hostname,
    key    => 'HostName',
    value  => $hostname,
    target => $ssh_conf_file,
  }

  ssh_config { "User ${hostname}":
    ensure => present,
    host   => $hostname,
    key    => 'User',
    value  => 'root',
    target => $ssh_conf_file,
  }

  ssh_config { "IdentityFile ${hostname}":
    ensure => present,
    host   => $hostname,
    key    => 'IdentityFile',
    value  => $key_pair_path,
    target => $ssh_conf_file,
  }

  ssh_config { "StrictHostKeyChecking ${hostname}":
    ensure => present,
    host   => $hostname,
    key    => 'StrictHostKeyChecking',
    value  => 'no',
    target => $ssh_conf_file,
  }

  ssh_config { "UserKnownHostsFile ${hostname}": 
    ensure => present,
    host   => $hostname,
    key    => 'UserKnownHostsFile',
    value  => '/dev/null',
    target => $ssh_conf_file,
  }

  ssh_config { "Port ${hostname}":
    ensure => present,
    host   => $hostname,
    key    => 'Port',
    value  => '22',
    target => $ssh_conf_file,
  }

  # Add the host as a Ceph host
  exec { "add ceph host ${hostname}":
    command => "ceph orch host add ${hostname} ${ip}",
    path    => '/usr/sbin:/usr/bin',
    onlyif  => "ssh ${hostname} '/bin/true'", 
    unless  => "ceph orch host ls | grep -w ${hostname}",
  }

  # Pass the Ceph management keyrings so we can manage the cluster from any node
  exec { "add ceph admin keyring ${hostname}":
    command     => "scp -i ${key_pair_path} /etc/ceph/ceph.client.admin.keyring ${hostname}:/etc/ceph",
    path        => '/usr/sbin:/usr/bin',
    subscribe   => Exec["add ceph host ${hostname}"],
    refreshonly => true,
  }

  exec { "add ceph public key ${hostname}":
    command     => "scp -i ${key_pair_path} /etc/ceph/ceph.pub ${hostname}:/etc/ceph",
    path        => '/usr/sbin:/usr/bin',
    subscribe   => Exec["add ceph host ${hostname}"],
    refreshonly => true,
  }

  exec { "add ceph conf ${hostname}": 
    command     => "scp -i ${key_pair_path} /etc/ceph/ceph.conf ${hostname}:/etc/ceph",
    path        => '/usr/sbin:/usr/bin',
    subscribe   => Exec["add ceph host ${hostname}"],
    refreshonly => true,
  }

  # Passes the script to add the remote host as an LXD cluster member
  exec { "add lxd script ${hostname}":
    command     => "scp -i ${key_pair_path} ${localdir}/bin/write_host_preseed.sh ${hostname}:${localdir}/bin",
    path        => '/usr/sbin:/usr/bin',
    subscribe   => Exec["add ceph host ${hostname}"],
    refreshonly => true,
  }

  $devices.each | String $device_path | {
    # creates OSD with the device on path $device_path if it does not exist
    exec { "configure device ${device_path} on host ${hostname}":
      command => "ceph orch daemon add osd ${hostname}:${device_path}",
      path    => '/usr/sbin:/usr/bin',
      onlyif  => ["ceph orch host ls | grep ${hostname}", "ssh ${hostname} '/bin/true'"],
      require => Exec["add ceph host ${hostname}"],
      unless  => "ssh ${hostname} 'ceph-volume lvm list ${device_path}'",
    }
  }
}
