class is_ceph_lxd::dependencies {
  # ntp package installed in node_base

  package { 'cephadm':
    ensure   => 'installed',
    provider => 'apt',
  }

  package { 'ceph-common':
    ensure   => 'installed',
    provider => 'apt',
  }

  package { 'docker.io':
    ensure   => 'installed',
    provider => 'apt',
  }

  package { 'ceph-osd':
    ensure   => 'installed',
    provider => 'apt',
  }
}
