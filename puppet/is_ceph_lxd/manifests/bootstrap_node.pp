class is_ceph_lxd::bootstrap_node(
  $public_network_address,
  $private_network_interface_address,
  $public_network_gateway,
  $public_network_interface,
  $backside_network_bridge_interface,
  $ceph_pool_pgs,
  $password, # Password used to authenticate hosts to lxd cluster, added to preseed template
  $lxd_version,
  $nodes,
  $ceph_pool_name                  = 'lxd-storage',
  $private_network_interface       = 'lxdbr0',
  $lxd_pool_name                   = 'default',
  $public_network_bridge_interface = 'br0',
  $dns_servers                     = ['8.8.8.8','1.1.1.1'],
  Array[String] $bootstrap_devices = [],
){

  $ip                = $nodes[$hostname]['ip']
  $hostname          = $facts['networking']['hostname']
  $backside_network_address = $nodes[$hostname]['backside_network_address']
  $dashboard_port    = '2211'
  $localdir          = lookup('filesystem::localdir')
  $preseed_path      = '/root/lxd_preseed.yaml'
  $script_path       = '/root/bootstrap_script.sh'
  $ceph_private_key  = '/etc/ceph/ceph'
  $ceph_hosts_script = '/root/check_ceph_hosts.sh'
  $key_pair_path     = '/etc/ceph/ceph'

  if length($password) == 0 {
    fail('Error: Password must not be empty')
  }

  class { 'is_ceph_lxd::dependencies': }
  class { 'is_lxd::lxd_server_base' :
    lxd_version                        => $lxd_version,
    lan_address                        => $public_network_address,
    private_network_interface          => $private_network_interface,
    private_address                    => $private_network_interface_address,
    backside_network_interface         => $nodes[$hostname]['backside_network_interface'],
    backside_network_bridge_interface  => $backside_network_bridge_interface,
    backside_network_address           => $backside_network_address,
    lan_network_interface              => $public_network_bridge_interface,
    gw4                                => $public_network_gateway,
    dns_servers                        => $dns_servers,
    primary_interface                  => $public_network_interface,
    storage_pool                       => $lxd_pool_name,
    clustering                         => true,
    secret_passwd                      => $password,
    ip                                 => $ip,
    require                            => Class['is_ceph_lxd::dependencies'],
  }

  file { $preseed_path :
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    content => template('is_ceph_lxd/bootstrap_preseed.yaml.erb'),
  }

  file { "${localdir}/bin/write_host_preseed.sh":
    ensure => 'file',
    owner  => 'root',
    group  => 'root',
    mode   => '0744',
    content => template('is_ceph_lxd/write_host_preseed.sh.erb'),
  }

  file { $script_path :
    source => 'puppet:///modules/is_ceph_lxd/bootstrap_script.sh',
    owner  => 'root',
    group  => 'root',
    mode   => '0744',
  }

  file { $ceph_hosts_script :
    source => 'puppet:///modules/is_ceph_lxd/check_ceph_hosts.sh',
    owner  => 'root',
    group  => 'root',
    mode   => '0744',
  }

  file { "${localdir}/bin/remove_cluster_node.sh" :
    source => 'puppet:///modules/is_ceph_lxd/remove_cluster_node.sh',
    owner  => 'root',
    group  => 'root',
    mode   => '0744',
  }

  exec { 'bootstrap ceph':
    command => "cephadm bootstrap --mon-ip ${backside_network_address} --skip-dashboard",
    path    => '/usr/sbin:/usr/bin',
    unless  => 'ceph -s',
    timeout => 1200,
    require => [Class['is_lxd::lxd_server_base'], Class['is_ceph_lxd::dependencies']],
  }

  # Create the private key to be used to access the Ceph services
  exec { 'create ceph private key':
    command => "ceph config-key get mgr/cephadm/ssh_identity_key > ${ceph_private_key}",
    path    => '/usr/bin',
    unless  => "test -e ${ceph_private_key}",
    require => Exec['bootstrap ceph'],
  }

  file { '/etc/ceph/ceph.pub':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
    require => Exec['bootstrap ceph'],
  }

  file { "${ceph_private_key}":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
    require => Exec['create ceph private key'],
  }

  # Change the default dashboard port to prevent overlapping port issue
  exec { 'change ceph dashboard port':
    command     => "ceph config set mgr mgr/dashboard/ssl_server_port ${dashboard_port}",
    path        => '/usr/sbin:/usr/bin',
    subscribe   => Exec['bootstrap ceph'],
    refreshonly => true,
  }

  # Disable and enable the dashboard for the changes to take effect
  exec { 'disable ceph dashboard':
    command     => 'ceph mgr module disable dashboard' ,
    path        => '/usr/sbin:/usr/bin',
    subscribe   => Exec['change ceph dashboard port'],
    refreshonly => true,
  }

  exec { 'enable ceph dashboard':
    command     => 'ceph mgr module enable dashboard' ,
    path        => '/usr/sbin:/usr/bin',
    subscribe   => Exec['disable ceph dashboard'],
    refreshonly => true,
  }

  $bootstrap_devices.each | String $device_path| {
    # creates OSD with the device on path $device_path if it does not exist
    exec { "add device $device_path":
      command => "ceph orch daemon add osd ${hostname}:${device_path}",
      path    => '/usr/sbin:/usr/bin',
      require => Exec['bootstrap ceph'],
      unless  => "ceph-volume lvm list | grep -w ${device_path}",
    }
  }

  # Extract the other hosts IPI addresses from the nodes hash
  $mon_ips = slice($nodes.values, 5)[0].map | $hash | { $hash['backside_network_address'] }.join(', ')

  file_line { 'ceph.conf':
    ensure  => present,
    path    => '/etc/ceph/ceph.conf',
    match   => '.*mon_host =.*',
    line    => "        mon_host = ${mon_ips}",
    require => Exec['bootstrap ceph'],
  }

  # Setup the host on the bootstrap node for root SSH access
  $nodes.each | String $host, Hash $node_data | {
    if $host != $hostname {
      is_ceph_lxd::host { $host :
        hostname => $host,
        ip       => $node_data['backside_network_address'],
        devices  => $node_data['devices'],
        require  => Exec['bootstrap ceph'],
      }
    }
  }

  $hosts = $nodes.keys()
  $hosts_without_bootstrap = $hosts.delete($hostname)
  $hosts_args = $hosts.join(' ')

  exec { 'create ceph pool':
    command => "ceph osd pool create ${ceph_pool_name} ${ceph_pool_pgs}",
    path    => '/usr/sbin:/usr/bin',
    unless  => "ceph osd lspools | grep ${ceph_pool_name}",
    onlyif  => "${ceph_hosts_script} ${hosts_args}",
    require => [Exec['bootstrap ceph'], File[$ceph_hosts_script], Is_ceph_lxd::Host[$hosts_without_bootstrap]],
    notify  => Exec['bootstrap LXD cluster'],
  }

  exec { 'bootstrap LXD cluster':
    command      => "${script_path} ${preseed_path}",
    path         => '/usr/bin:/snap/bin',
    require      => [Exec['create ceph pool'], File["$script_path"], File["$preseed_path"]],
    onlyif       => "test -e ${preseed_path} && test -e ${script_path}",
    unless       => 'lxc cluster list',
    refreshonly  => true,
  }

  # Give the cluster certificate to the other nodes after they're sucessfuly configured
  $nodes.each | String $host, Hash $node_data | {
    if $host != $hostname {
      exec { "add cluster cert ${host}":
        command => "scp -i ${key_pair_path} /var/snap/lxd/common/lxd/cluster.crt ${host}:/root",
        path    => '/usr/sbin:/usr/bin',
        onlyif  => "ssh ${host} '/bin/true'",
        unless  => "ssh ${host} test -e /root/cluster.crt",
        require => Exec['bootstrap LXD cluster'],
      }
    }
  }
}
