require 'json'

Facter.add(:lxd_pools) do
  setcode do
    storage_pools = Array.new
    output = `lxc storage list --format json 2>/dev/null`
    if $?.success?
      output = JSON.parse(output)
      output.each do |child|
        storage_pools.push(child['name'])
      end
    end
    storage_pools
  end
end
