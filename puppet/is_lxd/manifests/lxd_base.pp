class is_lxd::lxd_base (
  $lxd_version,
  Boolean $lxcfs_loadavg           = true,
  Boolean $is_stable               = false,
){

  $localdir = lookup('filesystem::localdir')

  file { "$localdir/bin/lxd-version-update.sh":
    ensure  => 'file',
    mode    => '744',
    owner   => 'root',
    group   => 'root',
    content => template('is_lxd/lxd-version-update.sh.erb'),
  }

  if !$is_stable {

    # refresh snapd version to 2.58 if necessary. This is from version 2.58 we have the --hold option that we are using to stabilize the installations
    exec { 'refresh snapd':
      command => 'snap refresh snapd',
      path    => '/usr/bin:/snap/bin',
      # This bash script will compare the version of snap with the target version. We have some backslashes to escape the single quote character because "--compare-version" doesn't accept it's arguments to be between double quotes
      onlyif => 'dpkg --compare-versions `snap list snapd | tail -1 | awk \'{print $2}\'` \'<<\' \'2.58\'',         # Executes only if snapd version is lower than 2.58
    }

    # remove unwanted version of lxd if present
    exec { 'remove LXD':
      command => 'snap remove lxd',
      path    => '/usr/bin:/snap/bin',
      onlyif  => 'snap list | grep lxd',                                       # Executes only if LXD is already installed
      unless  => "snap info lxd | grep tracking:.*${lxd_version}",             # Executes only if LXD isn't already the correct version
      require => Exec['refresh snapd'],
    }

    # install the $lxd_version of lxd
    exec { 'install LXD':
      command => "snap install lxd --channel=$lxd_version",
      path    => '/usr/bin:/snap/bin',
      unless  => 'snap list | grep lxd',                                       # Executes only if LXD is not installed
      require => Exec['remove LXD'],
    }

    # hold refreshes for lxd
    exec { 'hold LXD refreshes':
      command => 'snap refresh --hold lxd',
      path    => '/usr/bin:/snap/bin',
      unless => "snap list lxd | grep held",
      require => Exec['install LXD']
    }

    # Allows containers to report their own load average rather than the host's
    # This only applies on machine reboot as discussed here: https://discuss.linuxcontainers.org/t/state-of-per-container-load-average/13511/6
    exec { 'set lxcfs.loadavg':
      command => "snap set lxd lxcfs.loadavg=${lxcfs_loadavg}",
      path    => '/usr/bin:/snap/bin:/bin',
      unless  => "snap get lxd lxcfs.loadavg | grep -w ${lxcfs_loadavg}",  # execute only if lxcfs.loadavg is not set according to variable
      require => Exec['install LXD'],
    }

  }

}

