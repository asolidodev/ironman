class is_lxd::lxd_windows_image(
  $repo_url,
  $image_file,
  $username,
  $password,
){
  $localdir = lookup('filesystem::localdir')

  lxd::image { 'wserver19':
      ensure      => 'present',
      repo_url    => $repo_url,
      image_file  => $image_file,
      image_alias => 'wserver19',
      username    => $username,
      password    => $password,
  }

  exec { 'preload windows image':
    command     => "python3 $localdir/bin/vm-image-preload.py",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    subscribe   => Lxd::Image['wserver19'],
    timeout     => 3600,
    refreshonly => true,
  }
}

