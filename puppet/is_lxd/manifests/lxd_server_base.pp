class is_lxd::lxd_server_base(
  $gw4,
  $lan_address,
  $lan_network_interface,
  $lxd_version,
  $primary_interface,
  $private_network_interface,
  $private_address,
  $backside_network_interface             = undef,
  $backside_network_address               = undef,
  $backside_network_bridge_interface      = undef,
  $backside_network_netmask               = '255.255.255.0',             # /24 netmask by default unless specified otherwise
  $source                                 = undef,
  $storage_driver                         = undef, 
  $openvpn_ip                             = undef,
  $dual_network_profile_public_interface  = undef,
  Integer $vlan_id                        = 0,
  $storage_pool                           = 'default',
  $secret_passwd                          = '',
  $dns_servers                            = ['8.8.8.8','1.1.1.1'],
  $host_ip_address                        = $facts['networking']['ip'],
  Boolean $bridging                       = true,
  Boolean $setup_openvpn                  = false,                       # must be set to 'true' if $bridging is 'false'
  Boolean $clustering                     = false,
  Boolean $configure_vlan                 = false,
  Boolean $create_dual_network_profile    = false,
  Boolean $lxcfs_loadavg                  = true,
  Boolean $is_stable                      = false,
){

  $localdir                       = lookup('filesystem::localdir')
  $netmask                        = $facts['networking']['netmask']
  $prefix                         = ip_prefixlength("${host_ip_address}/${netmask}")    # used in LXD instance network script files
  $private_prefix                 = '24'  # the private network prefix by default is always /24 unless manually changed outside of Puppet
  $host_openvpn_network_interface = 'lxdbr1'

  class { 'is_lxd::lxd_base':
    lxd_version             => $lxd_version,
    lxcfs_loadavg           => $lxcfs_loadavg,
    is_stable               => $is_stable,
  }

  class { 'is_lxd::lxd_network': 
    gw4                               => $gw4,
    dns_servers                       => $dns_servers,
    lan_network_interface             => $lan_network_interface,
    private_network_interface         => $private_network_interface,
    private_address                   => $private_address,
    interface                         => $primary_interface,
    host_ip_address                   => $host_ip_address,
    backside_network_interface        => $backside_network_interface,
    backside_network_address          => $backside_network_address,
    backside_network_bridge_interface => $backside_network_bridge_interface,
    host_openvpn_network_interface    => $host_openvpn_network_interface,
    bridging                          => $bridging,
    configure_vlan                    => $configure_vlan,
    vlan_id                           => $vlan_id,
    require                           => Class['is_lxd::lxd_base']
  }

  class { '::lxd': 
    lxd_core_https_address_ensure  => 'present',
    lxd_core_trust_password_ensure => 'present',
    require                        => Class['is_lxd::lxd_network']
  }

  class { 'is_lxd::dependencies':  }

  package { 'ipcalc':
    ensure => installed,
  }

  file { ['/home/extras', '/home/extras/iso']:
    ensure => 'directory',
    owner  => 'root',
    group  => 'sudo',
    mode   => '770',
  }

  file { "$localdir/bin/vm_ip_check.py":
    ensure  => 'file',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('is_lxd/vm_ip_check.py.erb'),
  }

  file { "$localdir/bin/container-image-preload.py":
    ensure  => 'file',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('is_lxd/container-image-preload.py.erb'),
  }

  file { "$localdir/bin/vm-image-preload.py":
    ensure  => 'file',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('is_lxd/vm-image-preload.py.erb'),
  }

  file { "$localdir/bin/lxd-used-resources.py":
    source => 'puppet:///modules/is_lxd/lxd-used-resources.py',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { "$localdir/bin/lxd-backup.py":
    source => 'puppet:///modules/is_lxd/lxd-backup.py',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }
 
  file { "$localdir/bin/reload-snap-lxd-daemon.sh":
    source => 'puppet:///modules/is_lxd/reload-snap-lxd-daemon.sh',
    mode   => '0744',
    owner  => 'root',
    group  => 'root',
  }

  file { '/etc/systemd/system/vm_ip_check.service':
    ensure  => 'file',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('is_lxd/vm_ip_check.service.erb'),
  }

  file { "$localdir/bin/lxd-network.sh":
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    content => template('is_lxd/lxd-network.sh.erb'),
  }

  file { "$localdir/bin/lxd-private-network.sh":
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    content => template('is_lxd/lxd-private-network.sh.erb'),
  }

  file { "$localdir/bin/lxd-instance.sh":
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0744',
    content => template('is_lxd/lxd-instance.sh.erb'),
  }

  # Enable the service to probe for Windows VM IP addresses
  service { 'vm_ip_check':
    ensure  => running,
    enable  => true,
    require => [File["$localdir/bin/vm_ip_check.py"],File['/etc/systemd/system/vm_ip_check.service']],
  }

  if $clustering {

    # Make LXD use the external Ceph client instead of the outdated Snap packaged Ceph
    exec { 'lxd ceph external':
      command => 'snap set lxd ceph.external=true',
      path    => '/usr/bin:/usr/sbin:/snap/bin',
      unless  => 'snap get lxd ceph.external',
      notify  =>  Exec['restart LXD daemon'],
    }

    lxd::profile { 'lxdui-container-profile':
      ensure => 'present',
      config => {
        'user.network-config' => '
          #cloud-config
          version: 2
          ethernets:
            eth0:
              dhcp4: true
            eth1:
              dhcp4: true
              dhcp4-overrides:
                use-routes: false'
      },
      devices => {
        'eth0' => {
          'name'    => 'eth0',
          'nictype' => 'bridged',
          'parent'  => $lan_network_interface,
          'type'    => 'nic',
        },
        'eth1' => { 
          'name'    => 'eth1',
          'nictype' => 'bridged',
          'parent'  => $backside_network_bridge_interface,
          'type'    => 'nic',
        }
      },
      description => 'Profile to be used for the LXDUI instance only.',
    }

    file { "$localdir/bin/lxdui-network.sh":
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0744',
      content => template('is_lxd/lxdui-network.sh.erb'),
    }
  } else {
    if length($secret_passwd) == 0 {
      fail ('Error: Password must not be empty')
    }
  
    exec { 'set core.https_address':
      command => "lxc config set core.https_address $host_ip_address",
      path    => '/usr/bin:/usr/sbin:/bin:/snap/bin',
      unless  => "lxc config get core.https_address | grep -w $host_ip_address",    # exposes the new host IP address in case it has changed
      require => Class['is_lxd::lxd_base'],
    }
  
    exec { 'set core.trust_password':
      command     => "lxc config set core.trust_password $secret_passwd",
      path        => '/usr/bin:/usr/sbin:/bin:/snap/bin',
      subscribe   => Class['is_lxd::lxd_base'],
      refreshonly => true,
    }

    # Storage pool creation is handled my the cephadm module in a clustered environment
    if $storage_driver == 'zfs' {
      lxd::storage { $storage_pool :
        driver => $storage_driver,
        name   => $storage_pool,
        config => {
          'volatile.initial_source' => $source,
          'source'                  => $source,
          'zfs.pool_name'           => $storage_pool,
        },
        require => Class["Is_lxd::Lxd_base"],
      }
    } elsif $storage_driver == 'btrfs' {
      lxd::storage { $storage_pool :
        driver => $storage_driver,
        name   => $storage_pool,
        config => {
          'volatile.initial_source' => $source,
          'source'                  => $source,
        },
        require => Class["Is_lxd::Lxd_base"],
      }
    } elsif $storage_driver == 'lvm' {
      if $storage_pool in $lxd_pools {
        # set lvm.vg_name only after the storage pool has been created
        lxd::storage { $storage_pool :
          driver => $storage_driver,
          name   => $storage_pool,
          config => {
            'lvm.thinpool_name'  => 'LXDThinPool',
            'lvm.vg_name'        => $source,
            'source'             => $source,
            'lvm.vg.force_reuse' => 'true',
          },
          require => Class["Is_lxd::Lxd_base"],
        }
      } else {
        # if the storage pool is not yet created leave out lvm.vg_name as it is incompatible with source on creation
        lxd::storage { $storage_pool :
          driver => $storage_driver,
          name   => $storage_pool,
          config => {
            'lvm.thinpool_name'  => 'LXDThinPool',
            'source'             => $source,
            'lvm.vg.force_reuse' => 'true',
          },
          require => Class["Is_lxd::Lxd_base"],
        }
      }
    } else {
      lxd::storage { $storage_pool :
        driver => $storage_driver,
        name   => $storage_pool,
        config => {
          'volatile.initial_source' => $source,
          'source'                  => $source,
        },
        require => Class["Is_lxd::Lxd_base"],
      }
    }
  }

  # create profiles with the bridge interface if present
  if $bridging {
    class { 'is_lxd::lxd_bridge_profiles':
      lan_network_interface     => $lan_network_interface,
      private_network_interface => $private_network_interface,
    }

    # OpenVPN is optional in a bridged environment
    if $setup_openvpn {
      if ! $openvpn_ip {
        fail ('Error: OpenVPN IP address must be set if it is to be configured.')
      }

      class { 'is_lxd::lxd_openvpn_bridge_profile':
        lan_network_interface     => $lan_network_interface,
        private_network_interface => $private_network_interface,
        openvpn_ip                => $openvpn_ip,
        gateway                   => $gw4,
      }
    }
  } else {
    if ! $setup_openvpn {
      fail ('Error: OpenVPN is mandatory in a non bridged network environment.')
    }

    # OpenVPN is mandatory for remote instance access in a non bridged environment
    class { 'is_lxd::lxd_openvpn_private_profile':
      private_network_interface      => $private_network_interface,
      host_openvpn_network_interface => $host_openvpn_network_interface,
    }
  }

  if $create_dual_network_profile {
    if ! $dual_network_profile_public_interface {
      fail('$dual_network_profile_public_interface must be passed when creating the dual network profile profile.')
    }

    lxd::profile { 'public-backside-network-profile':
      ensure => 'present',
      config => {
        'user.network-config' => '
          #cloud-config
          version: 2
          ethernets:
            eth0:
              dhcp4: true
            eth1:
              dhcp4: true
              dhcp4-overrides:
                use-routes: false'
      },
      devices => {
        'eth0' => {
          'name'    => 'eth0',
          'nictype' => 'bridged',
          'parent'  => $dual_network_profile_public_interface,
          'type'    => 'nic',
        },
        'eth1' => {
          'name'    => 'eth1',
          'nictype' => 'bridged',
          'parent'  => $backside_network_bridge_interface,
          'type'    => 'nic',
        }
      },
      description => 'Profile to be used for the LXDUI instance only.',
    }
  }

  # always create the private network profiles
  class { 'is_lxd::lxd_private_profiles':
    private_network_interface => $private_network_interface,
  }
  
  # Restart the LXD daemon to either generate xtables firewall rules or set external Ceph
  exec { 'restart LXD daemon':
    command     => 'systemctl restart snap.lxd.daemon',
    path        => '/bin',
    refreshonly => true,
  }  
}
