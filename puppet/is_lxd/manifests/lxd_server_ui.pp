class is_lxd::lxd_server_ui(
  String $group                    ='sudo',
  $lxd_version,
  $remote_address,
  $secret_passwd,
  Boolean $show_options            = false,
){
  
  include stdlib
  include apt

  $dependencies      = ['git','build-essential','libssl-dev','python3-venv','python3-pip','python3-dev','bridge-utils']
  $ui_base_path      = '/root/puppet'
  $lxd_remote_url    = "https://${remote_address}:8443/"
  $ssl_verify        = 'false'
  $lxd_remote_name   = 'host'

  # the automatically generated client certificate and key are present in the following paths
  $lxd_ssl_cert_path = '/root/snap/lxd/common/config/client.crt'
  $lxd_ssl_key_path  = '/root/snap/lxd/common/config/client.key'

  class { 'is_lxd::lxd_base':
    lxd_version             => $lxd_version,
  }

  # apt is updated within is_puppet_base/packages_base.pp
  package {$dependencies: ensure => 'installed', provider => 'apt', require => Class['is_lxd::lxd_base']}

  # executes only if a 'host' remote does not exists
  -> exec { 'add_host':
    command => "lxc remote add ${lxd_remote_name} ${remote_address} --password ${secret_passwd} --accept-certificate",
    path    => '/usr/bin:/usr/sbin:/bin:/snap/bin',
    unless  => "lxc remote list | grep -w ${lxd_remote_name}"
  }
  
  # executes only if the current remote IP address is different from the provided IP address
  exec { 'override_host':
    command => "lxc remote set-url ${lxd_remote_name} ${lxd_remote_url}",
    path    => '/usr/bin:/usr/sbin:/bin:/snap/bin',
    unless  => "lxc remote list | grep -w ${lxd_remote_name} | grep -w ${remote_address}",
    require => Exec['add_host'],
  }

  vcsrepo { "$ui_base_path":
    ensure   => latest,
    provider => git,
    revision => 'master',
    source   => 'https://bitbucket.org/asolidodev/ironman.git',
    require  => Exec['add_host'],
  }

  file { "${ui_base_path}/lxdui/app/__metadata__.py":
    ensure  => 'file',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('is_lxd/__metadata__.py.erb'),
    require => Vcsrepo["$ui_base_path"],
    notify  => Service['lxdui'],
  }

  if !$show_options {
    
    file { "$ui_base_path/lxdui/app/ui/templates/index.html":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/is_lxd/index-no-options.html',
      require => File["${ui_base_path}/lxdui/app/__metadata__.py"],
    }

    file { "$ui_base_path/lxdui/app/ui/blueprint.py":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/is_lxd/blueprint-no-options.py',
      require => File["$ui_base_path/lxdui/app/ui/templates/index.html"]
    }

  } else {
    
    file { "$ui_base_path/lxdui/app/ui/templates/index.html":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/is_lxd/index.html',
      require => File["$ui_base_path/lxdui/app/__metadata__.py"],
    }

    # needed for switching from no options version
    file { "$ui_base_path/lxdui/app/ui/blueprint.py":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/is_lxd/blueprint.py',
      require => File["$ui_base_path/lxdui/app/ui/templates/index.html"]
    }
  }

  file { '/etc/systemd/system/lxdui.service':
    content  => template('is_lxd/lxdui.service.erb'),
    owner    => 'root',
    group    => 'root',
    mode     => '0644',
    require  => File["$ui_base_path/lxdui/app/ui/blueprint.py"],
  }

  service { 'lxdui':
    ensure  => running,
    enable  => true,
    require => File['/etc/systemd/system/lxdui.service'],
  }
}
