class is_lxd::dependencies {

  # installs remote-viewer necessary for windows VM configuration
  package { 'virt-viewer':
    ensure   => 'installed',
    provider => 'apt',
  }

  # installs nmap, used for host discovery
  package { 'nmap':
    ensure   => 'installed',
    provider => 'apt',
  }

  # installs nftables in order to flush any rules created by LXD
  package { 'nftables':
    ensure   => 'installed',
    provider => 'apt',
  }

  # installs the python package manager
  package { 'python3-pip':
    ensure   => 'installed',
    provider => 'apt',
  }

  # installs the python LXD API
  package { 'pylxd':
    ensure   => '2.2.11',
    provider => 'pip3',
    require  => Package['python3-pip'],
  }

  # installs the python nmap library
  package { 'python3-nmap':
    ensure   => 'installed',
    provider => 'pip3',
    require  => [Package['nmap'], Package['python3-pip']],
  }
}
