class is_lxd::lxd_private_profiles (
  String $private_network_interface,
){
  lxd::profile { 'container-private':
    ensure  => 'present',
    config  => {
      'user.network-config' => '
        #cloud-config
        version: 1
        config:
          - type: physical
            name: eth0
            subnets:
              - type: dhcp
                ipv4: true',
    },
    devices => {
      'eth0' => {
        'name'    => 'eth0',
        'nictype' => 'bridged',
        'parent'  => $private_network_interface,
        'type'    => 'nic',
      },
    },
    description => 'Profile to be used for a container on a private network only.',
  }

  lxd::profile { 'vm-private':
    ensure  => 'present',
    config  => {
      'user.network-config' => '
        #cloud-config
        version: 1
        config:
          - type: physical
            name: enp5s0
            subnets:
              - type: dhcp
                ipv4: true',
    },
    devices => {
      'eth0' => {
        'name'    => 'eth0',
        'nictype' => 'bridged',
        'parent'  => $private_network_interface,
        'type'    => 'nic',
      },
      'config' => {
        'source' => 'cloud-init:config',
        'type'   => 'disk',
      },
    },
    description => 'Profile to be used for a VM on a private network only.',
  }

}
