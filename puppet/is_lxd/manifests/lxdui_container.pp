class is_lxd::lxdui_container(
  $ui_main_address,
  $netmask,
  $gateway_address,
  $pool_name,
  $dns,
  $cpu                   = '2',
  $memory                = '2GB',
  $ui_lxd_subnet_address = undef,
  $target                = undef,
  Boolean $clustering    = false,
  Boolean $bridging      = true,
){

  $localdir = lookup('filesystem::localdir')

  if $clustering {
    if $ui_lxd_subnet_address == undef {
      fail ('$ui_lxd_subnet_address must be declared on clustered LXDUI container creation.')
    }

    if $target == undef {
      fail ('$target must be declared on clustered LXDUI container creation.')
    }

    lxd::container { 'lxdui-container':
      state          => 'started',
      type           => 'container',
      profiles       => ['lxdui-container-profile'],
      config         => { 'limits.cpu' => $cpu, 'limits.memory' => $memory, },
      devices        => {
        'root' => {
          'path' => '/',
          'pool' => $pool_name,
          'size' => '5GB',
          'type' => 'disk',
        },
      },
      image          => 'ubuntu2004',
      set_ip         => false,
      set_private_ip => false,
      target         => $target,
    }

    $parsed_dns = join($dns,' ')

    # The network settings of the LXDUI container in a clustered differ as it needs to
    # have access to the secondary private network interface which does not exist in a standalone server setup
    exec { 'set LXDUI network':
      command => "$localdir/bin/lxdui-network.sh lxdui-container $ui_main_address $ui_lxd_subnet_address $netmask $gateway_address $parsed_dns",
      require => Lxd::Container['lxdui-container']
    }
  } elsif ! $bridging {
    lxd::container { 'lxdui-container':
      state                  => 'started',
      type                   => 'container',
      profiles               => ['container-private'],
      config                 => { 'limits.cpu' => $cpu, 'limits.memory' => $memory, },
      devices                => {
        'root' => { 
          'path' => '/',
          'pool' => $pool_name,
          'size' => '5GB',
          'type' => 'disk',
        },
      },
      image                  => 'ubuntu2004',
      set_ip                 => false,
      set_private_ip         => true,
      private_ip_address     => $ui_main_address,
      private_netmask        => $netmask,
      private_gateway        => $gateway_address,
      private_dns            => $dns,
    }
  } else {
    lxd::container { 'lxdui-container':
      state          => 'started',
      type           => 'container',
      profiles       => ['container-lan'],
      config         => { 'limits.cpu' => $cpu, 'limits.memory' => $memory, },
      devices        => {
        'root' => {
          'path' => '/',
          'pool' => $pool_name,
          'size' => '5GB',
          'type' => 'disk',
        },
      },
      image          => 'ubuntu2004',
      set_ip         => true,
      set_private_ip => false,
      ip_address     => $ui_main_address,
      netmask        => $netmask,
      gateway        => $gateway_address,
      dns            => $dns,
    }
  }
}
