class is_lxd::lxd_standard_containers {
  $localdir = lookup('filesystem::localdir')


  lxd::image { 'ubuntu2204':
      ensure      => 'official',
      image_name  => 'ubuntu:22.04',
      image_alias => 'ubuntu2204',
  }

  lxd::image { 'ubuntu2004':
      ensure      => 'official',
      image_name  => 'ubuntu:20.04',
      image_alias => 'ubuntu2004',
  }

  lxd::image { 'ubuntu1804':
      ensure      => 'official',
      image_name  => 'ubuntu:18.04',
      image_alias => 'ubuntu1804',
  }

  # Preload the images for the first time on the storage pool
  # This dramatically decreases the first instance creation time
  exec { 'preload linux container images':
    command     => "python3 $localdir/bin/container-image-preload.py",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    subscribe   => [Lxd::Image['ubuntu2204'],Lxd::Image['ubuntu2004'],Lxd::Image['ubuntu1804']],
    timeout     => 3600,
    refreshonly => true,
  }
}
