class is_lxd::lxd_standard_vms {

  $localdir = lookup('filesystem::localdir')


  lxd::image { 'ubuntu2204vm':
      ensure      => 'official-vm',
      image_name  => 'ubuntu:22.04',
      image_alias => 'ubuntu2204vm'
  }

  lxd::image { 'ubuntu2004vm':
      ensure      => 'official-vm',
      image_name  => 'ubuntu:20.04',
      image_alias => 'ubuntu2004vm'
  }

  # Preload the image for the first time on the storage pool
  # This dramatically decreases the first instance creation time
  exec { 'preload linux vm images':
    command     => "python3 $localdir/bin/vm-image-preload.py",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    subscribe   => [Lxd::Image['ubuntu2204vm'],Lxd::Image['ubuntu2004vm']],
    timeout     => 3600,
    refreshonly => true,
  }
}
