class is_lxd::lxd_bridge_profiles (
  String $lan_network_interface,
  String $private_network_interface,
){
  lxd::profile { 'container-lan-private':
    ensure  => 'present',
    config  => {
      'user.network-config' => '
        #cloud-config
        version: 2
        ethernets:
          eth0:
            dhcp4: true
          eth1:
            dhcp4: true
            dhcp4-overrides:
              use-routes: false'
    },
    devices => {
      'eth0' => {
        'name'    => 'eth0',
        'nictype' => 'bridged',
        'parent'  => $lan_network_interface,
        'type'    => 'nic',
      },
      'eth1' => {
        'name'    => 'eth1',
        'nictype' => 'bridged',
        'parent'  => $private_network_interface,
        'type'    => 'nic',
      }
    },
    description => 'Profile to be used for a container on a private network and the LAN.',
  }

  lxd::profile { 'vm-lan-private':
    ensure  => 'present',
    config  => {
      'user.network-config' => '
        #cloud-config
        version: 2
        ethernets:
          enp5s0:
            dhcp4: true
          enp6s0:
            dhcp4: true
            dhcp4-overrides:
              use-routes: false'
    },
    devices => {
      'eth0' => {
        'name'    => 'eth0',
        'nictype' => 'bridged',
        'parent'  => $lan_network_interface,
        'type'    => 'nic',
      },
      'eth1' => {
        'name'    => 'eth1',
        'nictype' => 'bridged',
        'parent'  => $private_network_interface,
        'type'    => 'nic',
      },
      'config' => {
        'source' => 'cloud-init:config',
        'type'   => 'disk',
      },
    },
    description => 'Profile to be used for a VM on a private network and the LAN.',
  }

  lxd::profile { 'container-lan':
    ensure  => 'present',
    config  => {
      'user.network-config' => '
        #cloud-config
        version: 1
        config:
          - type: physical
            name: eth0
            subnets:
              - type: dhcp
                ipv4: true',
    },
    devices => {
      'eth0' => {
        'name'    => 'eth0',
        'nictype' => 'bridged',
        'parent'  => $lan_network_interface,
        'type'    => 'nic',
      },
    },
    description => 'Profile to be used for a container on the LAN only.',
  }

  lxd::profile { 'vm-lan':
    ensure  => 'present',
    config  => {
      'user.network-config' => '
        #cloud-config
        version: 1
        config:
          - type: physical
            name: enp5s0
            subnets:
              - type: dhcp
                ipv4: true',
    },
    devices => {
      'eth0' => {
        'name'    => 'eth0',
        'nictype' => 'bridged',
        'parent'  => $lan_network_interface,
        'type'    => 'nic',
      },
      'config' => {
        'source' => 'cloud-init:config',
        'type'   => 'disk',
      },
    },
    description => 'Profile to be used for a VM on the LAN only.',
  }
}
