class is_lxd::lxd_openvpn_bridge_profile (
  String $lan_network_interface,
  String $private_network_interface,
  String $openvpn_ip,
  String $gateway,
){
  lxd::profile { 'openvpn-vm':
    ensure  => 'present',
    config  => {
      'user.network-config' => "
        #cloud-config
        version: 1
        config:
          - type: physical
            name: enp5s0
            subnets:
              - type: static
                address: ${openvpn_ip}
                gateway: ${gateway}
                dns_nameservers:
                  - 8.8.8.8",
    },
    devices => {
      'eth0' => {
        'name'    => 'eth0',
        'nictype' => 'bridged',
        'parent'  => $lan_network_interface,
        'type'    => 'nic',
      },
      'eth1' => {
        'name'    => 'eth1',
        'nictype' => 'bridged',
        'parent'  => $private_network_interface,
        'type'    => 'nic',
      },
      'config' => {
        'source' => 'cloud-init:config',
        'type'   => 'disk',
      },
    },
    description => 'Profile to be used for a VM used to configure an OpenVPN server to access the private LXD network.',
  }
}
