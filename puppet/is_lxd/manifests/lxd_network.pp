class is_lxd::lxd_network ( 
  $gw4,
  $lan_network_interface,
  $private_network_interface,
  $private_address,
  $interface,
  $host_ip_address,
  $host_openvpn_network_interface,
  $backside_network_interface         = undef,
  $backside_network_address           = undef,
  $backside_network_bridge_interface  = undef,
  $backside_network_netmask           = '255.255.255.0',  # /24 netmask by default unless specified otherwise
  $dns_servers                        = ['8.8.8.8','1.1.1.1'],
  Integer $vlan_id                    = 0,
  Boolean $bridging                   = true,
  Boolean $configure_vlan             = false,
){
  $netmask = $facts['networking']['netmask']
  $prefix = ip_prefixlength("${host_ip_address}/${netmask}")

  if $configure_vlan and ! $vlan_id {
    fail('VLAN ID must be set when configuring a VLAN interface.')
  }

  if ! $bridging {
    # In this scenario we can only access the instances via VPN access to the private LXD network
    # To have a functioning VPN instance we must create a secondary private bridge interface to
    # route requests from the host to the VPN instance via port forwarding on port 1194
    exec { 'create openvpn private network':
      command => "lxc network create $host_openvpn_network_interface ipv4.address='10.99.99.1/24' ipv4.nat=true ipv6.address=none ipv6.nat=false",
      path    => '/snap/bin:/usr/bin',
      unless  => "lxc network info $host_openvpn_network_interface",
    }
  }

  # Configure both a public facing and a private network interface
  # Usually only used in a clustered setup
  if $backside_network_interface and $backside_network_address and $backside_network_bridge_interface {

    $backside_prefix = ip_prefixlength("${backside_network_address}/${backside_network_netmask}")

    if $configure_vlan and ! $bridging{
      class { 'netplan':
        config_file   => '/etc/netplan/50-cloud-init.yaml',
        ethernets     => { 
          $interface                  => { 
            'dhcp4' => false
          },
          $backside_network_interface => { 
            'dhcp4' => false,
          } 
        },
        vlans         => {
          "${interface}.${vlan_id}" => {
            'id'        => $vlan_id,
            'link'      => $interface,
            'addresses' => ["${host_ip_address}/${prefix}"],
            'gateway4'  => $gw4,
            'nameservers' => {
              'addresses' => $dns_servers,
            },
          },
        },
        bridges       => { 
          $backside_network_bridge_interface => { 
            'dhcp4'       => false,
            'interfaces'  => [$backside_network_interface],
            'addresses'   => ["${backside_network_address}/${backside_prefix}"],
            'nameservers' => { 
              'addresses' => $dns_servers,
            },
          } 
        },
        netplan_apply => true,
      }
    } elsif $configure_vlan and $bridging {
      fail ('Bridging with VLAN configuration is not yet supported')
    } elsif ! $configure_vlan and ! $bridging {
      class { 'netplan':
        config_file   => '/etc/netplan/50-cloud-init.yaml',
        ethernets     => {
          $interface                  => {
            'dhcp4'       => false,
            'addresses'   => ["${host_ip_address}/${prefix}"],
            'gateway4'    => $gw4,
            'nameservers' => {
              'addresses' => $dns_servers,
            },
          },
          $backside_network_interface => {
            'dhcp4' => false,
          }
        },
        bridges       => {
          $backside_network_bridge_interface => {
            'dhcp4'       => false,
            'interfaces'  => [$backside_network_interface],
            'addresses'   => ["${backside_network_address}/${backside_prefix}"],
            'nameservers' => {
              'addresses' => $dns_servers,
            },
          }
        },
        netplan_apply => true,
      }
    } elsif ! $configure_vlan and $bridging {
      class { 'netplan':
        config_file   => '/etc/netplan/50-cloud-init.yaml',
        ethernets     => {
          $interface                  => {
            'dhcp4' => false
          },
          $backside_network_interface => {
            'dhcp4' => false,
          }
        },
        bridges       => {
          $lan_network_interface             => {
            'dhcp4'       => false,
            'interfaces'  => [$interface],
            'addresses'   => ["${host_ip_address}/${prefix}"],
            'gateway4'    => $gw4,
            'nameservers' => {
              'addresses' => $dns_servers,
            },
          },
          $backside_network_bridge_interface => {
            'dhcp4'       => false,
            'interfaces'  => [$backside_network_interface],
            'addresses'   => ["${backside_network_address}/${backside_prefix}"],
            'nameservers' => {
              'addresses' => $dns_servers,
            },
          }
        },
        netplan_apply => true,
      }
    }
  } elsif ! $bridging and $configure_vlan {
    # network configuration with a VLAN for public IP and without bridging

    class { 'netplan':
      config_file   => '/etc/netplan/50-cloud-init.yaml',
      ethernets     => {
        $interface  => {
          'dhcp4' => false,
        },
      },
      vlans         => {
        "${interface}.${vlan_id}" => {
          'id'        => $vlan_id,
          'link'      => $interface,
          'addresses' => ["${host_ip_address}/${prefix}"],
          'gateway4'  => $gw4,
          'nameservers' => {
            'addresses' => $dns_servers,
          },
        },
      },
      require       => Exec['create openvpn private network'],
      netplan_apply => true,
    }
  } elsif ! $bridging and ! $configure_vlan{
    # network configuration with neither VLAN nor bridging

    class { 'netplan':
      config_file   => '/etc/netplan/50-cloud-init.yaml',
      ethernets     => {
        $interface  => {
          'dhcp4'       => false,
          'addresses'   => ["${host_ip_address}/${prefix}"],
          'gateway4'    => $gw4,
          'nameservers' => {
            'addresses' => $dns_servers,
          },
        },
      },
      netplan_apply => true,
      require       => Exec['create openvpn private network'],
    }
  } elsif $bridging and $configure_vlan {
    fail ('Bridging with VLAN configuration is not yet supported')
  } elsif $bridging and ! $configure_vlan {
    # default case with bridging and without VLAN
    class { 'netplan':
      config_file   => '/etc/netplan/50-cloud-init.yaml',
      ethernets     => {
        $interface  => {
          'dhcp4' => false
        }
      },
      bridges       => {
        $lan_network_interface => {
          'dhcp4'       => false,
          'interfaces'  => [$interface],
          'addresses'   => ["${host_ip_address}/${prefix}"],
          'gateway4'    => $gw4,
          'nameservers' => {
            'addresses' => $dns_servers,
          },
        },
      },
      netplan_apply => true,
    }
  }

  exec { 'create private network':
    command => "lxc network create $private_network_interface ipv4.address=$private_address ipv4.nat=true ipv6.address=none ipv6.nat=false",
    path    => '/snap/bin:/usr/bin',
    unless  => "lxc network set $private_network_interface ipv4.address=$private_address ipv4.nat=true ipv6.address=none ipv6.nat=false",
    require => Class[netplan],
  }

  # Purge nftables rules if any are present
  # This will force LXD to use xtables instead of nftables
  # xtables is the firewall driver used by Puppet managed firewall rules
  # Reference: https://github.com/lxc/lxd/blob/master/lxd/firewall/firewall_load.go
  exec { 'purge nftables rules':
    command => 'nft flush ruleset',
    path    => '/usr/sbin:/usr/bin:/bin',
    onlyif  => 'nft list ruleset | grep .',       # only flush rules if they exist
    notify  => Exec['restart LXD daemon'],
    require => [Package['nftables'], Exec['create private network']],
  }
}
