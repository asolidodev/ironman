import sys
import os
import datetime
from os import path
from pylxd import Client # using pylxd 2.2.11

STOP_STATE='Stopped'

try:
    instance_name = sys.argv[1]
    path_to_backup = sys.argv[2]
except:
    sys.exit('ERROR: The script should ne executed as follows: python3 lxd-backup.py INSTANCE_NAME /path/to/backup')


if not path.isdir(path_to_backup):
    sys.exit("ERROR: '{0}' is not a valid directory path".format(path_to_backup))

client = Client()

try:
    instance = client.api.instances[instance_name].get().json()
except:
    sys.exit("ERROR: Instance '{0}' does not exist.".format(instance_name))

instance_state = instance['metadata']['status']

if instance_state != STOP_STATE:
    sys.exit("ERROR: Instance '{0}' must be stopped.".format(instance_name))

images = []
for image in client.api.images.get().json()['metadata']:
    images.append(client.api.images[image.split('/')[-1]].get().json()['metadata'])

# if error, then backup does not exist, redirect error output
os.system("lxc image delete {0}-backup 2> /dev/null".format(instance_name))


print("Creating backup...")
os.system("lxc publish {0} --alias {0}-backup".format(instance_name))

print("Exporting backup...")
dt = datetime.datetime.today()
year = dt.year
month = dt.month
day = dt.day
hour = dt.hour
minute = dt.minute
os.system("lxc image export {0}-backup {1}/{0}-backup-{2}{3}{4}{5}{6}".format(instance_name,path_to_backup,year,month,day,hour,minute))