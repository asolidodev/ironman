#!/usr/bin/env python3

from pylxd import Client # using pylxd 2.2.11
import os
import socket
import sys

RUN_STATE='Running'

client = Client()
instances = client.instances.all()

return_value = os.system('lxc cluster list >/dev/null 2>&1')

if return_value == 0:
    total_disk = 0
    total_running_disk = 0

    os.system('lxc list -c ns4L,limits.memory:MEM,limits.cpu:CPU,devices:root.size:DISK')

    # supresses the LXD api call console output
    hosts = {}
    sys.stdout = open(os.devnull, "w")
    for host in client.cluster.get().members.all():
        hosts[host.server_name] = {
            'total_cpu': 0,
            'total_memory': 0,
            'total_disk': 0,
            'running_cpu': 0,
            'running_memory': 0,
            'running_disk': 0,
        }
    sys.stdout = sys.__stdout__

    hostname = socket.gethostname()
    for instance in instances:
        name = instance.name
        status = instance.status
        memory = instance.config.get('limits.memory')
        cpu = instance.config.get('limits.cpu')
        location = instance.location

        # calculate allocated CPUs on node
        if cpu is None:
            print("WARNING: Instance {0} CPU not set.".format(name))
        else:
            cpu = int(cpu)
            hosts[location]['total_cpu'] = hosts[location]['total_cpu'] + cpu
            if status == RUN_STATE:
                hosts[location]['running_cpu'] = hosts[location]['running_cpu'] + cpu

        # calculate memory used on node
        if memory is None:
            print("WARNING: Instance {0} memory not set.".format(name))
        else:
            if "GB" in memory:
                memory = int(memory.replace('GB',''))
                hosts[location]['total_memory'] = hosts[location]['total_memory'] + memory

            elif "MB" in memory:
                memory = int(memory.replace('MB','')) / 1000
                hosts[location]['total_memory'] = hosts[location]['total_memory'] + memory
            else:
                raise Exception('ERROR: Unknown data size unit.')

            if status == RUN_STATE:
                hosts[location]['running_memory'] = hosts[location]['running_memory'] + memory

        # calculate disk space used on node
        try:
            disk = instance.devices.get('root')['size']
            if "GB" in disk:
                disk = int(disk.replace('GB',''))
                hosts[location]['total_disk'] = hosts[location]['total_disk'] + disk
            elif "MB" in disk:
                disk = int(disk.replace('MB','')) / 1000
                hosts[location]['total_disk'] = hosts[location]['total_disk'] + disk
            else:
                raise Exception('ERROR: Unknown data size unit.')

            if status == RUN_STATE:
                hosts[location]['running_disk']  = hosts[location]['running_disk'] + disk
        except:
            print("WARNING: Instance {0} disk device not configured.".format(name))

        # calculate total storage pool usage
        try:
            disk = instance.devices.get('root')['size']
            if "GB" in disk:
                disk = int(disk.replace('GB',''))
                total_disk = total_disk + disk
            elif "MB" in disk:
                disk = int(disk.replace('MB','')) / 1000
                total_disk = total_disk + disk
            else:
                raise Exception('ERROR: Unknown data size unit.')
            if status == RUN_STATE:
                total_running_disk = total_running_disk + disk
        except Exception:
            print("WARNING: Instance {0} disk device not configured.".format(name))

    for host in hosts:
        print('+--------------------------------------------------------------+')
        print('Host {0} used resources:'.format(host))
        print('     - Running instances:')
        print('         - CPU    : {0} cores'.format(hosts[host]['running_cpu']))
        print('         - Memory : {0} GB'.format(hosts[host]['running_memory']))
        print('         - Disk   : {0} GB'.format(hosts[host]['running_disk']))
        print('     - All instances:')
        print('         - CPU    : {0} cores'.format(hosts[host]['total_cpu']))
        print('         - Memory : {0} GB'.format(hosts[host]['total_memory']))
        print('         - Disk   : {0} GB'.format(hosts[host]['total_disk']))

    print('+--------------------------------------------------------------+')
    print('Total cluster disk usage                  : {0} GB'.format(total_disk))
    print('Total cluster running instance disk usage : {0} GB'.format(total_running_disk))
    print('+--------------------------------------------------------------+')

else:
    total_cpu = 0
    total_disk = 0
    total_memory = 0

    running_cpu = 0
    running_disk = 0
    running_memory = 0
    os.system('lxc list -c ns4,limits.memory:MEM,limits.cpu:CPU,devices:root.size:DISK')

    print()

    for instance in instances:
        name = instance.name
        memory = instance.config.get('limits.memory')
        cpu = instance.config.get('limits.cpu')
        status = instance.status

        # calculate allocated CPUs
        if cpu is None:
            print("WARNING: Instance {0} CPU not set.".format(name))
        else:
            cpu = int(cpu)
            total_cpu = total_cpu + cpu
            if status == RUN_STATE:
                running_cpu = running_cpu + cpu

        # calculate memory used
        if memory is None:
            print("WARNING: Instance {0} memory not set.".format(name))
        else:
            if "GB" in memory:
                memory = int(memory.replace('GB',''))
                total_memory = total_memory + memory

            elif "MB" in memory:
                memory = int(memory.replace('MB','')) / 1000
                total_memory = total_memory + memory
            else:
                raise Exception('ERROR: Unknown data size unit.')

            if status == RUN_STATE:
                running_memory = running_memory + memory

        # calculate disk space used
        try:
            disk = instance.devices.get('root')['size']
            if "GB" in disk:
                disk = int(disk.replace('GB',''))
                total_disk = total_disk + disk
            elif "MB" in disk:
                disk = int(disk.replace('MB','')) / 1000
                total_disk = total_disk + disk
            else:
                raise Exception('ERROR: Unknown data size unit.')

            if status == RUN_STATE:
                running_disk = running_disk + disk
        except:
            print("WARNING: Instance {0} disk device not configured.".format(name))

    print()
    print('+--------------------------------------------------------------+')
    print('Total CPU allocation                : {0} cores'.format(total_cpu))
    print('Total disk allocation               : {0}GB'.format(total_disk))
    print('Total memory allocation             : {0}GB'.format(total_memory))
    print('+--------------------------------------------------------------+')
    print('Running instances CPU allocation    : {0} cores'.format(running_cpu))
    print('Running instances disk allocation   : {0}GB'.format(running_disk))
    print('Running instances memory allocation : {0}GB'.format(running_memory))
    print('+--------------------------------------------------------------+')
