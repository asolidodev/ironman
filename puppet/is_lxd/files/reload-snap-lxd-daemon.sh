#!/bin/bash

# This script will attempt to reload the snap LXD daemon until successful or an execution limit is reached
TIMEOUT=10
COUNTER=0
EXEC_LIMIT=10

until systemctl reload snap.lxd.daemon.service
do
  sleep $TIMEOUT
  COUNTER=$(($COUNTER + 1))
  if [ $COUNTER -ge $EXEC_LIMIT ]; then
    exit 1
  fi
done

exit 0
