#!/bin/bash

UBUNTU_CONTAINER_IMAGE='ubuntu2004'
UBUNTU_VM_IMAGE='ubuntu2004vm'
STORAGE_POOL='lxd-pool'

# Number of Ubuntu 20.04 containers to be launched for testing
NUM_CONTAINERS=20
# Number of container loop cycles to perform
NUM_CONTAINER_LOOPS=2
UBUNTU_CONTAINER_NAME='ubuntu-container-test'
CONTAINER_PROFILE='bridge-cli'
CONTAINER_STORAGE_SIZE='10'

# Number of Ubuntu 20.04 VMs to be launched for testing
NUM_VMS=10
NUM_VM_LOOPS=1
UBUNTU_VM_NAME='ubuntu-vm-test'
VM_PROFILE='vm'
VM_STORAGE_SIZE='50'


WINDOWS_VM_NAME='windows-vm-test'
WINDOWS_VM_CPU=4
WINDOWS_VM_MEMORY=4
WINDOWS_VM_STORAGE_POOL='default2'
WINDOWS_VM_STORAGE_SIZE=25
WINDOWS_VM_PROFILE='default'
WINDOWS_IMAGE_NAME='wserver19'
WINDOWS_SLEEP_TIME=10

# Number of Windows VM cycles to be performed
NUM_WINDOWS_VM_LOOPS=2

DELAY=2

SLEEP_TIME=120

TOTAL_CREATE_RUNTIME=0
TOTAL_SHUTDOWN_RUNTIME=0
TOTAL_BOOT_RUNTIME=0
TOTAL_REBOOT_RUNTIME=0
TOTAL_DELETE_RUNTIME=0

# ============================================================================ Container tests

curr_loop=0
while [ "$curr_loop" -lt "$NUM_CONTAINER_LOOPS" ]; do
   
    echo "Started container creation"
    i=0
    while [ "$i" -lt "$NUM_CONTAINERS" ]; do
        CREATE_START=`date +%s.%N`
        lxc init $UBUNTU_CONTAINER_IMAGE $UBUNTU_CONTAINER_NAME-$i -p $CONTAINER_PROFILE -s $STORAGE_POOL
        lxc config device set $UBUNTU_CONTAINER_NAME-$i root size=${CONTAINER_STORAGE_SIZE}GB
        CREATE_END=`date +%s.%N`
        sleep $DELAY
        TOTAL_CREATE_RUNTIME=$( echo "$CREATE_END - $CREATE_START + $TOTAL_CREATE_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended container creation"

    echo "Started container boot"
    i=0
    while [ "$i" -lt "$NUM_CONTAINERS" ]; do
        BOOT_START=`date +%s.%N`
        lxc start $UBUNTU_CONTAINER_NAME-$i
        BOOT_END=`date +%s.%N`
	sleep $DELAY
        TOTAL_BOOT_RUNTIME=$( echo "$BOOT_END - $BOOT_START  + $TOTAL_BOOT_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended container boot"
    
    sleep $SLEEP_TIME

    echo "Started container reboot"
    i=0
    while [ "$i" -lt "$NUM_CONTAINERS" ]; do
        REBOOT_START=`date +%s.%N`
        lxc restart $UBUNTU_CONTAINER_NAME-$i
        REBOOT_END=`date +%s.%N`
	sleep $DELAY
        TOTAL_REBOOT_RUNTIME=$( echo "$REBOOT_END - $REBOOT_START  + $TOTAL_REBOOT_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended container reboot"
        
    sleep $SLEEP_TIME

    echo "Started container shutdown"
    i=0
    while [ "$i" -lt "$NUM_CONTAINERS" ]; do
        SHUTDOWN_START=`date +%s.%N`
        lxc stop $UBUNTU_CONTAINER_NAME-$i
        SHUTDOWN_END=`date +%s.%N`
	sleep $DELAY
        TOTAL_SHUTDOWN_RUNTIME=$( echo "$SHUTDOWN_END - $SHUTDOWN_START + $TOTAL_SHUTDOWN_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended container shutdown"

    
    echo "Started container deletion"
    i=0
    while [ "$i" -lt "$NUM_CONTAINERS" ]; do
        DELETE_START=`date +%s.%N`
        lxc delete $UBUNTU_CONTAINER_NAME-$i
        DELETE_END=`date +%s.%N`
	sleep $DELAY
        TOTAL_DELETE_RUNTIME=$( echo "$DELETE_END - $DELETE_START  + $TOTAL_DELETE_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended container deletion"

    curr_loop=$(($curr_loop + 1))
done

TOTAL_CONTAINER_RUNS=$(($NUM_CONTAINERS * $NUM_CONTAINER_LOOPS))

CONTAINER_CREATE_AVG=$( echo "scale=3; $TOTAL_CREATE_RUNTIME / $TOTAL_CONTAINER_RUNS" | bc -l)
CONTAINER_SHUTDOWN_AVG=$( echo "scale=3; $TOTAL_SHUTDOWN_RUNTIME / $TOTAL_CONTAINER_RUNS" | bc -l)
CONTAINER_BOOT_AVG=$( echo "scale=3; $TOTAL_BOOT_RUNTIME / $TOTAL_CONTAINER_RUNS" | bc -l)
CONTAINER_REBOOT_AVG=$( echo "scale=3; $TOTAL_REBOOT_RUNTIME / $TOTAL_CONTAINER_RUNS" | bc -l)
CONTAINER_DELETE_AVG=$( echo "scale=3; $TOTAL_DELETE_RUNTIME / $TOTAL_CONTAINER_RUNS" | bc -l)

# ============================================================================ VM tests

TOTAL_CREATE_RUNTIME=0
TOTAL_SHUTDOWN_RUNTIME=0
TOTAL_BOOT_RUNTIME=0
TOTAL_REBOOT_RUNTIME=0
TOTAL_DELETE_RUNTIME=0

curr_loop=0
while [ "$curr_loop" -lt "$NUM_VM_LOOPS" ]; do
    echo "Started VM creation"
    i=0
    while [ "$i" -lt "$NUM_VMS" ]; do
        CREATE_START=`date +%s.%N`
        lxc init $UBUNTU_VM_IMAGE $UBUNTU_VM_NAME-$i -p $VM_PROFILE -s $STORAGE_POOL --vm
        lxc config device set $UBUNTU_VM_NAME-$i root size=${VM_STORAGE_SIZE}GB
        CREATE_END=`date +%s.%N`
        sleep $DELAY
        TOTAL_CREATE_RUNTIME=$( echo "$CREATE_END - $CREATE_START + $TOTAL_CREATE_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended VM creation"
   
    echo "Started VM boot"
    i=0
    while [ "$i" -lt "$NUM_VMS" ]; do
        BOOT_START=`date +%s.%N`
        lxc start $UBUNTU_VM_NAME-$i
        BOOT_END=`date +%s.%N`
	sleep $DELAY
        TOTAL_BOOT_RUNTIME=$( echo "$BOOT_END - $BOOT_START  + $TOTAL_BOOT_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended VM boot"

    sleep $SLEEP_TIME
    
    echo "Started VM reboot"
    i=0
    while [ "$i" -lt "$NUM_VMS" ]; do
        echo $i
	REBOOT_START=`date +%s.%N`
        lxc restart $UBUNTU_VM_NAME-$i
        REBOOT_END=`date +%s.%N`
	sleep $DELAY
        TOTAL_REBOOT_RUNTIME=$( echo "$REBOOT_END - $REBOOT_START  + $TOTAL_REBOOT_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended VM reboot"
    
    # If the containers are stopped too soon after rebooting, they may still be in a 'Stopped' state
    # as lxc restart sends a stop event followed by a start event
    sleep $SLEEP_TIME

    echo "Started VM shutdown"
    i=0
    while [ "$i" -lt "$NUM_VMS" ]; do
        SHUTDOWN_START=`date +%s.%N`
        lxc stop $UBUNTU_VM_NAME-$i --verbose
        SHUTDOWN_END=`date +%s.%N`
	sleep $DELAY
        TOTAL_SHUTDOWN_RUNTIME=$( echo "$SHUTDOWN_END - $SHUTDOWN_START + $TOTAL_SHUTDOWN_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended VM shutdown"
   
    sleep 10
    echo "Started VM deletion"
    i=0
    while [ "$i" -lt "$NUM_VMS" ]; do
        DELETE_START=`date +%s.%N`
        lxc delete $UBUNTU_VM_NAME-$i
        DELETE_END=`date +%s.%N`
	sleep $DELAY
        TOTAL_DELETE_RUNTIME=$( echo "$DELETE_END - $DELETE_START  + $TOTAL_DELETE_RUNTIME" | bc -l )
        i=$(($i + 1))
    done
    echo "Ended VM deletion"
    
    curr_loop=$(($curr_loop + 1))
done

TOTAL_VM_RUNS=$(($NUM_VMS * $NUM_VM_LOOPS))

VM_CREATE_AVG=$( echo "scale=3; $TOTAL_CREATE_RUNTIME / $TOTAL_VM_RUNS" | bc -l)
VM_SHUTDOWN_AVG=$( echo "scale=3; $TOTAL_SHUTDOWN_RUNTIME / $TOTAL_VM_RUNS" | bc -l)
VM_BOOT_AVG=$( echo "scale=3; $TOTAL_BOOT_RUNTIME / $TOTAL_VM_RUNS" | bc -l)
VM_REBOOT_AVG=$( echo "scale=3; $TOTAL_REBOOT_RUNTIME / $TOTAL_VM_RUNS" | bc -l)
VM_DELETE_AVG=$( echo "scale=3; $TOTAL_DELETE_RUNTIME / $TOTAL_VM_RUNS" | bc -l)

# ============================================================================ Windows tests

TOTAL_CREATE_RUNTIME=0
TOTAL_SHUTDOWN_RUNTIME=0
TOTAL_BOOT_RUNTIME=0
TOTAL_REBOOT_RUNTIME=0
TOTAL_DELETE_RUNTIME=0

i=0
while [ "$i" -lt "$NUM_WINDOWS_VM_LOOPS" ]; do

    echo "Started Windows VM creation"
    CREATE_START=`date +%s.%N`
    lxc init $WINDOWS_IMAGE_NAME $WINDOWS_VM_NAME --vm -c security.secureboot=false -c limits.cpu=$WINDOWS_VM_CPU -c limits.memory=${WINDOWS_VM_MEMORY}GB -p $WINDOWS_VM_PROFILE -s $WINDOWS_VM_STORAGE_POOL
    lxc config device set $WINDOWS_VM_NAME root size=${WINDOWS_VM_STORAGE_SIZE}GB
    lxc start $WINDOWS_VM_NAME
    CREATE_END=`date +%s.%N`
    
    # a wait is not needed for Windows VM shutdown tests as we pass the force option
    # since Windows VMs can not be shutdown through LXD normally

    echo "Started Windows VM shutdown"
    SHUTDOWN_START=`date +%s.%N`
    lxc stop $WINDOWS_VM_NAME --force
    SHUTDOWN_END=`date +%s.%N`
    echo "Ended Windows VM shutdown"

    echo "Start boot"
    BOOT_START=`date +%s.%N`
    lxc start $WINDOWS_VM_NAME
    BOOT_END=`date +%s.%N`
    echo "End boot"

    echo "Start reboot"
    REBOOT_START=`date +%s.%N`
    lxc restart $WINDOWS_VM_NAME --force
    REBOOT_END=`date +%s.%N`
    echo "End reboot"

    lxc stop $WINDOWS_VM_NAME --force

    DELETE_START=`date +%s.%N`
    lxc delete $WINDOWS_VM_NAME
    DELETE_END=`date +%s.%N`

    TOTAL_CREATE_RUNTIME=$( echo "$CREATE_END - $CREATE_START + $TOTAL_CREATE_RUNTIME" | bc -l )
    TOTAL_SHUTDOWN_RUNTIME=$( echo "$SHUTDOWN_END - $SHUTDOWN_START + $TOTAL_SHUTDOWN_RUNTIME" | bc -l )
    TOTAL_BOOT_RUNTIME=$( echo "$BOOT_END - $BOOT_START  + $TOTAL_BOOT_RUNTIME" | bc -l )
    TOTAL_REBOOT_RUNTIME=$( echo "$REBOOT_END - $REBOOT_START  + $TOTAL_REBOOT_RUNTIME" | bc -l )
    TOTAL_DELETE_RUNTIME=$( echo "$DELETE_END - $DELETE_START  + $TOTAL_DELETE_RUNTIME" | bc -l )
    i=$(($i + 1))
done

WINDOWS_VM_CREATE_AVG=$( echo "scale=3; $TOTAL_CREATE_RUNTIME / $NUM_WINDOWS_VM_LOOPS" | bc -l)
WINDOWS_VM_SHUTDOWN_AVG=$( echo "scale=3; $TOTAL_SHUTDOWN_RUNTIME / $NUM_WINDOWS_VM_LOOPS" | bc -l)
WINDOWS_VM_BOOT_AVG=$( echo "scale=3; $TOTAL_BOOT_RUNTIME / $NUM_WINDOWS_VM_LOOPS" | bc -l)
WINDOWS_VM_REBOOT_AVG=$( echo "scale=3; $TOTAL_REBOOT_RUNTIME / $NUM_WINDOWS_VM_LOOPS" | bc -l)
WINDOWS_VM_DELETE_AVG=$( echo "scale=3; $TOTAL_DELETE_RUNTIME / $NUM_WINDOWS_VM_LOOPS" | bc -l)


echo "============================== Container results =============================="
echo "Container Storage size: ${CONTAINER_STORAGE_SIZE}GB"
echo "Average container creation time   ($TOTAL_CONTAINER_RUNS runs) :  $CONTAINER_CREATE_AVG s"
echo "Average container shutdown time   ($TOTAL_CONTAINER_RUNS runs) :  $CONTAINER_SHUTDOWN_AVG s"
echo "Average container boot time       ($TOTAL_CONTAINER_RUNS runs) :  $CONTAINER_BOOT_AVG s"
echo "Average container reboot time     ($TOTAL_CONTAINER_RUNS runs) :  $CONTAINER_REBOOT_AVG s"
echo "Average container delete time     ($TOTAL_CONTAINER_RUNS runs) :  $CONTAINER_DELETE_AVG s"
echo "==============================    VM  Results    =============================="
echo "VM Storage size: ${VM_STORAGE_SIZE}GB"
echo "Average VM creation time          ($TOTAL_VM_RUNS runs) :  $VM_CREATE_AVG s"
echo "Average VM shutdown time          ($TOTAL_VM_RUNS runs) :  $VM_SHUTDOWN_AVG s"
echo "Average VM boot time              ($TOTAL_VM_RUNS runs) :  $VM_BOOT_AVG s"
echo "Average VM reboot time            ($TOTAL_VM_RUNS runs) :  $VM_REBOOT_AVG s"
echo "Average VM delete time            ($TOTAL_VM_RUNS runs) :  $VM_DELETE_AVG s"
echo "============================= Windows  VM Results ============================="
echo "Average Windows VM creation time  ($NUM_WINDOWS_VM_LOOPS runs) :  $WINDOWS_VM_CREATE_AVG s"
echo "Average Windows VM shutdown time  ($NUM_WINDOWS_VM_LOOPS runs) :  $WINDOWS_VM_SHUTDOWN_AVG s"
echo "Average Windows VM boot time      ($NUM_WINDOWS_VM_LOOPS runs) :  $WINDOWS_VM_BOOT_AVG s"
echo "Average Windows VM reboot time    ($NUM_WINDOWS_VM_LOOPS runs) :  $WINDOWS_VM_REBOOT_AVG s"
echo "Average Windows VM delete time    ($NUM_WINDOWS_VM_LOOPS runs) :  $WINDOWS_VM_DELETE_AVG s"
