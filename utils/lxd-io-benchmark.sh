INSTANCE_NAME="vm-test"
DIR="/root"
COUNT="100k"
BS="64k"
FIO_FILE_NAME=".fiomark.tmp"
TOTAL_LOOPS=15

CURR_LOOP=1
CURR_READ=0
CURR_WRITE=0
TOTAL_READ=0
TOTAL_WRITE=0

lxc exec $INSTANCE_NAME -- apt update
lxc exec $INSTANCE_NAME -- apt install fio

lxc exec $INSTANCE_NAME -- dd if=/dev/urandom of=$DIR/input bs="$BS" count="$COUNT"

while [ "$CURR_LOOP" -le "$TOTAL_LOOPS" ]; do
    lxc exec $INSTANCE_NAME -- "sync; echo 3 | tee /proc/sys/vm/drop_caches"
    sync; echo 3 > /proc/sys/vm/drop_caches
    write_result=$(lxc exec $INSTANCE_NAME -- dd if=$DIR/input of=$DIR/test bs="$BS" count="$COUNT" conv=fdatasync 2>&1)
    CURR_WRITE=$(echo $write_result | grep -o -E '[0-9]+.[0-9]+ MB/s' | sed 's/ MB\/s//')
    TOTAL_WRITE=$( echo "$TOTAL_WRITE + $CURR_WRITE" | bc -l)
    curr_avg=$( echo "scale=3; $TOTAL_WRITE / $CURR_LOOP" | bc -l)
    echo "Current dd write average is:  $curr_avg MB/s"
    lxc exec $INSTANCE_NAME -- rm $DIR/test
    CURR_LOOP=$(($CURR_LOOP + 1))
done

AVG_WRITE=$( echo "scale=3; $TOTAL_WRITE / $TOTAL_LOOPS" | bc -l)
echo "dd write average: $AVG_WRITE"

CURR_LOOP=1
CURR_READ=0
CURR_WRITE=0
TOTAL_READ=0
TOTAL_WRITE=0

while [ "$CURR_LOOP" -le "$TOTAL_LOOPS" ]; do
    lxc exec $INSTANCE_NAME -- "sync; echo 3 | tee /proc/sys/vm/drop_caches"
    sync; echo 3 > /proc/sys/vm/drop_caches
    read_result=$(lxc exec $INSTANCE_NAME -- dd if=$DIR/input of=/dev/null bs="$BS" count="$COUNT" 2>&1)
    CURR_READ=$(echo $read_result | grep -o -E '[0-9]+.[0-9]+ MB/s' | sed 's/ MB\/s//')
    TOTAL_READ=$( echo "$TOTAL_READ + $CURR_READ" | bc -l)
    curr_avg=$( echo "scale=3; $TOTAL_READ / $CURR_LOOP" | bc -l)
    echo "Current dd read average is:  $curr_avg MB/s"
    CURR_LOOP=$(($CURR_LOOP + 1))
done

AVG_READ=$( echo "scale=3; $TOTAL_READ / $TOTAL_LOOPS" | bc -l)
echo "dd read average: $AVG_READ"

CURR_LOOP=1
CURR_READ=0
CURR_WRITE=0
TOTAL_READ=0
TOTAL_WRITE=0

while [ "$CURR_LOOP" -le "$TOTAL_LOOPS" ]; do
    lxc exec $INSTANCE_NAME -- "sync; echo 3 > /proc/sys/vm/drop_caches"
    sync; echo 3 > /proc/sys/vm/drop_caches
    # use the same file generated with dd for read tests
    read_result=$(lxc exec $INSTANCE_NAME -- fio --loops=1 --filename="$DIR/input" --stonewall --ioengine=libaio --direct=1 --zero_buffers=0 --name=SeqQ32T1read --bs=$BS --iodepth=32 --numjobs=1 --rw=read --refill_buffers=1 --end_fsync=1  2>&1)
    CURR_READ=$(echo $read_result | grep -o -E '[0-9]+.[0-9]+MB/s' | head -1 | sed 's/MB\/s//')
    TOTAL_READ=$( echo "$TOTAL_READ + $CURR_READ" | bc -l)
    curr_avg=$( echo "scale=3; $TOTAL_READ / $CURR_LOOP" | bc -l)
    echo "Current fio read average is:  $curr_avg MB/s"
    CURR_LOOP=$(($CURR_LOOP + 1))
    #lxc exec $INSTANCE_NAME -- rm .fiomark2.tmp
done

AVG_READ=$( echo "scale=3; $TOTAL_READ / $TOTAL_LOOPS" | bc -l)
echo "fio read average: $AVG_READ"

lxc exec $INSTANCE_NAME -- rm $DIR/input

CURR_LOOP=1
CURR_READ=0
CURR_WRITE=0
TOTAL_READ=0
TOTAL_WRITE=0

while [ "$CURR_LOOP" -le "$TOTAL_LOOPS" ]; do
    lxc exec $INSTANCE_NAME -- "sync; echo 3 > /proc/sys/vm/drop_caches"
    sync; echo 3 > /proc/sys/vm/drop_caches
    # create a new file with the same size
    write_result=$(lxc exec $INSTANCE_NAME -- fio --loops=1 --size=10GB --filename="$FIO_FILE_NAME" --stonewall --ioengine=libaio --direct=1 --zero_buffers=0 --name=SeqQ32T1write --bs=$BS --iodepth=32 --numjobs=1 --rw=write --end_fsync=1 2>&1)
    CURR_WRITE=$(echo $write_result | grep -o -E '[0-9]+.[0-9]+MB/s' | head -1 | sed 's/MB\/s//')
    TOTAL_WRITE=$( echo "$TOTAL_WRITE + $CURR_WRITE" | bc -l)
    curr_avg=$( echo "scale=3; $TOTAL_WRITE / $CURR_LOOP" | bc -l)
    echo "Current fio write average is:  $curr_avg MB/s"
    CURR_LOOP=$(($CURR_LOOP + 1))
    lxc exec $INSTANCE_NAME -- rm $FIO_FILE_NAME
done


AVG_WRITE=$( echo "scale=3; $TOTAL_WRITE / $TOTAL_LOOPS" | bc -l)
echo "fio write  average: $AVG_WRITE"
