from jsonschema import validate, ValidationError

schema = {
    "oneOf": [
        {"$ref": "#/definitions/singleObject"}, # plain object
        {
            "type": "array", # array of plain objects
            "items": {"$ref": "#/definitions/singleObject"}
        }
    ],
    "definitions": {
        "singleObject": {
            'type':'object',
            'required': ['name', 'image'],
            'properties':{
                'name':{
                    'type':'string',
                    'description':'Container name'
                },
                'image':{
                    'type':'string',
                    'description':'Image alias or hash'
                },
                'type':{
                    'type':'string',
                    'description':'Type of instance'
                },
                'newName': {
                    'type': 'string',
                    'description': 'New Container name'
                },
                'stateful':{
                    'type':'boolean',
                    'description':'Stateful container'
                },
                'profiles':{
                    'type':'array',
                    'items':[
                        {'type':'string'}
                    ]
                },
                'network': {
                    'type': 'array',
                    'items': [
                        {'type': 'string'}
                    ]
                },
                'target': {
                    'type': 'string',
                    'description': 'Target location of the instance if in a clustered environment'
                },
                'cpu': {
                    'type': 'object',
                    'description': 'CPU Limitation',
                    'required':['cores'],
                    'properties':{
                        'cores':{
                            'type':'integer',
                            'description':'Set the number of CPU cores',
                            'minimum':1
                        }
                    }
                },
                'memory':{
                    'type': 'object',
                    'description': 'Memory limitation',
                    'required': ['sizeInMB'],
                    'properties': {
                        'sizeInMB': {
                            'type': 'integer',
                            'description': 'Set memory limitation',
                            'minimum': 32
                        },
                    }
                },
                'autostart':{
                    'type':'boolean',
                    'description':'autostart instance'
                },
                'description': {
                    'type': 'string',
                    'description': 'Description instance'
                }
            }
        }
    }
}

copyMoveSchema = {
    "oneOf": [
        {"$ref": "#/definitions/singleObject"}, # plain object
    ],
    "definitions": {
        "singleObject": {
            'type':'object',
            'required': ['newContainer'],
            'properties':{
                'newContainer':{
                    'type':'string',
                    'description':'newContainer (name)'
                }
            }
        }
    }
}

instanceConfigurationSchema = {
    "oneOf": [
        {"$ref": "#/definitions/singleObject"}, # plain object
    ],
    "definitions": {
        "singleObject": {
            'type':'object',
            'required': ['newInstance'],
            'properties':{
                'newInstance':{
                    'type':'string',
                    'description':'newInstance (name)'
                }
            }
        }
    }
}

migrateSchema = {
    "oneOf": [
        {"$ref": "#/definitions/singleObject"}, # plain object
    ],
    "definitions": {
        "singleObject": {
            'type':'object',
            'required': ['target'],
            'properties':{
                'target':{
                    'type':'string',
                    'description':'Target destination host'
                }
            }
        }
    }
}

exportSchema = {
    "oneOf": [
        {"$ref": "#/definitions/singleObject"}, # plain object
    ],
    "definitions": {
        "singleObject": {
            'type':'object',
            'required': ['imageAlias'],
            'properties':{
                'imageAlias':{
                    'type':'string',
                    'description':'image (alias)'
                }
            }
        }
    }
}

def doValidateImageExport(input):
    try:
        validate(input, exportSchema)
        return None
    except ValidationError as e:
        return e

def doValidateCloneMove(input):
    try:
        validate(input, copyMoveSchema)
        return None
    except ValidationError as e:
        return e

def doValidateInstanceConfigurationClone(input):
    try:
        validate(input, instanceConfigurationSchema)
        return None
    except ValidationError as e:
        return e

def doValidateMigrate(input):
    try:
        validate(input, migrateSchema)
        return None
    except ValidationError as e:
        return e

def doValidate(input):
    try:
        validate(input, schema)
        return None
    except ValidationError as e:
        return e
