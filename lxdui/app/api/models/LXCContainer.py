from app.api.models.LXDModule import LXDModule
from pylxd import Client
from app.lib.conf import Config
from app import __metadata__ as meta
import logging
import re
import os

logging = logging.getLogger(__name__)

class LXCContainer(LXDModule):
    def __init__(self, input):
        self.data = {}
        self.remoteHost = '127.0.0.1'

        if not input.get('name'):
            logging.error('Instance name is required for any instance operation')
            raise ValueError('Missing instance name.')

        self.setName(input.get('name'))
        logging.info('Connecting to LXD')
        super(LXCContainer, self).__init__(remoteHost=self.remoteHost)

        verifyName(input.get('name'))

        if self.client.instances.exists(self.data.get('name')):
            existing = self.info()
            self.data['config'] = existing['config']
            self.data['devices'] = existing['devices']
            # store instance info data to reduce API calls
            self.data['info'] = existing

        if input.get('image'):
            self.setImageType(input.get('image'))

        if input.get('profiles'):
            self.setProfile(input.get('profiles'))

        if input.get('ephemeral'):
            self.setEphemeral(input.get('ephemeral'))

        if input.get('description'):
            self.setDescription(input.get('description'))

        if input.get('cpu'):
            self.setCPU(input.get('cpu'))

        if input.get('memory'):
            self.setMemory(input.get('memory'))

        if input.get('newContainer'):
            self.setNewContainer(input.get('newContainer'))

        if input.get('newInstance'):
            self.setNewInstance(input.get('newInstance'))

        if input.get('imageAlias'):
            self.setImageAlias(input.get('imageAlias'))

        if input.get('autostart') != None:
            self.setBootType(input.get('autostart'))
        else:
            self.setBootType(True)

        if input.get('stateful') != None:
            self.setEphemeral(not input.get('stateful'))
        else:
            self.setEphemeral(False)

        if input.get('newName'):
            self.setNewName(input.get('newName'))

        if input.get('config'):
            self.setConfig(input.get('config'))
        
        if input.get('devices'):
            self.setDevices(input.get('devices'))

        if input.get('target'):
            self.setTarget(input.get('target'))


    def setImageType(self, input):
        # Detect image type (alias or fingerprint)
        logging.debug('Checking if image {} exists'.format(input))
        tempImageType = self.hasImage(input)

        if not tempImageType:
            logging.error('Image with alias or fingerprint {} not found'.format(input))
            raise ValueError('Image with alias or fingerprint {} not found'.format(input))

        if not self.data.get('source'):
            self.data['source']={'type':'image'}
        self.data['source'][tempImageType] = input


    def setName(self, input):
        logging.debug('Setting image name to {}'.format(input))
        self.data['name'] = input

    def setDescription(self, input):
        logging.debug('Setting image description as {}'.format(input))
        self.data['description'] = input

    def setProfile(self, input):
        logging.debug('Setting image profiles as {}'.format(input))
        self.data['profiles']=input

    def setEphemeral(self, input):
        logging.debug('Setting image ephemeral type to {}'.format(input))
        self.data['ephemeral']=input

    def initConfig(self):
        if not self.data.get('config', None):
            self.data['config']={}

    def setCPU(self, input):
        self.initConfig()
        if input.get('cores'):
            logging.debug('Set CPU count to {}'.format(input.get('cores')))
            self.data['config']['limits.cpu']='{}'.format(input.get('cores'))

    def setMemory(self, input):
        self.initConfig()
        self.data['config']['limits.memory']='{}MB'.format(input.get('sizeInMB'))
        self.data['config']['limits.memory.enforce'] = 'hard'
        logging.debug('Memory limit set to {} with restrictions set to {}'.format(
            self.data['config']['limits.memory'],
            self.data['config']['limits.memory.enforce']
        ))

    def setDevices(self, input):
        self.data['devices'] = input

    def setTarget(self, input):
        self.data['target'] = input

    def setNewContainer(self, input):
        self.data['newContainer'] = input

    def setNewInstance(self, input):
        self.data['newInstance'] = input

    def setImageAlias(self, input):
        logging.debug('Setting image alias as {}'.format(input))
        self.data['imageAlias'] = input

    def setBootType(self, input):
        self.initConfig()
        self.data['config']['boot.autostart'] = '1' if input else '0'
        logging.debug('Setting autostart boot type to {}'.format(input))

    def setEphemeral(self, input):
        self.initConfig()
        self.data['ephemeral'] = input
        logging.debug('Setting container as ephemeral {}'.format(input))

    def setNewName(self, input):
        self.initConfig()
        logging.debug('Setting new container name as: {}'.format(input))
        self.data['newName'] = input

    def setConfig(self, input):
        logging.debug('Setting key-value for container config')
        self.initConfig()
        for key in input.keys():
            self.data['config'][key] = input[key]

    def info(self):
        try:

            logging.info('Reading instance {} information'.format(self.data.get('name')))
            c = self.client.instances.get(self.data.get('name'))
            container = self.client.api.instances[self.data.get('name')].get().json()['metadata']
            container['cpu'] = c.state().cpu
            container['memory'] = c.state().memory
            container['network'] = c.state().network
            container['processes'] = c.state().processes
            container['pid'] = c.state().pid
            container['disk'] = c.state().disk

            return container
        except Exception as e:
            logging.error('Failed to retrieve information for instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def getInfo(self):
        try:
            return self.data['info']
        except Exception as e:
            logging.error('Failed to get instance {} information'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def create(self, waitIt=True):
        try:
            instanceType = ''
            for image in LXDModule().listLocalImages():
                if(image["fingerprint"] == self.data['source']['fingerprint']):
                    instanceType = image["type"]
                    break
                
            logging.info('Creating instance {}'.format(self.data.get('name')))
            self.data['type'] = instanceType
            if 'target' in self.data:
                self.client.instances.create(self.data, wait=waitIt, target=self.data['target'])
            else:
                self.client.instances.create(self.data, wait=waitIt)

            if self.data['config']['boot.autostart'] == '1':
                self.start(waitIt)
            return self.info()
        except Exception as e:
            logging.error('Failed to create instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def delete(self, force=False):
        try:
            logging.info('Deleting instance with {} enforcement set to {}'.format(self.data.get('name'), force))
            container = self.client.instances.get(self.data.get('name'))
            if self.info().get('ephemeral'):
                container.stop(wait=True)
                return
            elif force and self.info().get('status') == 'Running':
                container.stop(wait=True)
            container.delete()
        except Exception as e:
            logging.error('Failed to delete instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def update(self):
        try:
            logging.info('Updating instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            if self.data.get('config'):
                container.config = self.data.get('config')

            if self.data.get('profiles'):
                container.profiles = self.data.get('profiles')

            if self.data.get('description'):
                container.description = self.data.get('description')

            container.save(True)
            if self.data.get('newName'):
                self.rename()
            return self.info()
        except Exception as e:
            logging.error('Failed to update instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def start(self, waitIt=True):
        try:
            logging.info('Starting instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            container.start(wait=waitIt)
        except Exception as e:
            logging.error('Failed to start instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def stop(self, waitIt=True):
        try:
            logging.info('Stopping instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            container.stop(wait=waitIt)
        except Exception as e:
            logging.error('Failed to stop instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def restart(self, waitIt=True):
        try:
            logging.info('Restarting instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            container.restart(wait=waitIt)
        except Exception as e:
            logging.error('Failed to restart instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def clone(self):
        try:
            logging.info('Cloning instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            if container.status == 'Running':
                container.stop(wait=True)

            copyData = container.generate_migration_data()
            
            # remove all MAC address data to prevent conflicting MAC address errors
            copyData['config'].pop('volatile.eth0.hwaddr', None)
            copyData['config'].pop('volatile.eth1.hwaddr', None)
            
            copyData['source'] = {'type': 'copy', 'source': self.data.get('name')}
            copyData['name'] = self.data.get('newContainer')

            newContainer = self.client.instances.create(copyData, wait=True)
            container.start(wait=True)
            newContainer.start(wait=True)
            return self.client.api.instances[self.data.get('newContainer')].get().json()['metadata']
        except Exception as e:
            logging.error('Failed to clone instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def cloneConfiguration(self):
        try:
            logging.info('Cloning instance {} configuration'.format(self.data.get('name')))
            instance = self.client.instances.get(self.data.get('name'))
            newInstanceData = {}
            image_fingerprint = instance.config['volatile.base_image']
            newInstanceConfig = extractConfig(instance.config)

            # to avoid calling the 'generate_migration_data' method from pylxd we manually set the data fields
            # this method requires the original instance to be stopped which should not be necessary
            # when cloning only the configuration options
            newInstanceData['config'] = newInstanceConfig
            newInstanceData['profiles'] = instance.profiles
            newInstanceData['devices'] = instance.devices
            newInstanceData['name'] = self.data.get('newInstance')
            newInstanceData['source'] = {'type': 'image', 'fingerprint': image_fingerprint}

            newInstance = self.client.instances.create(newInstanceData, wait=True)
            newInstance.start(wait=True)
            return self.client.api.instances[self.data.get('newInstance')].get().json()['metadata']
        except Exception as e:
            logging.error('Failed to clone instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def move(self):
        try:
            logging.info('Moving instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            container.rename(self.data.get('newContainer'), wait=True)
            return self.client.api.instances[self.data.get('newContainer')].get().json()['metadata']
        except Exception as e:
            logging.error('Failed to move instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def migrate(self):
        try:
            logging.info('Migrating instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            if container.status == 'Running':
                raise Exception("Failed to migrate instance {}. Instance is still running".format(self.data.get('name')))

            return_value = 1
            try:
                return_value = os.system("lxc move {}:{} --target {}".format(Config().get(meta.APP_NAME,'lxdui.lxd.remote.name'), self.data.get('name'), self.data.get('target')))
            except:
                return_value = os.system("lxc move {} --target {}".format(self.data.get('name'), self.data.get('target')))
            
            if return_value != 0:
                raise Exception("Failed to migrate instance {}".format(self.data.get('name')))
            else:
                return self.info()
        except Exception as e:
            logging.error('Failed to migrate instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def export(self, force=False):
        try:
            logging.info('Exporting instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            if force and container.status == 'Running':
                container.stop(wait=True)

            image = container.publish(wait=True)
            image.add_alias(self.data.get('imageAlias'), self.data.get('name'))
            try:
                fingerprint = container.config.get('volatile.base_image')
                self.client.api.images[image.fingerprint].put(json={'properties': self.client.api.images[fingerprint].get().json()['metadata']['properties']})
            except:
                logging.error('Image does not exist.')
            container.start(wait=True)
            return self.client.api.images[image.fingerprint].get().json()['metadata']
        except Exception as e:
            logging.error('Failed to export instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def rename(self, force=True):
        try:
            logging.info('Renaming instance {} to {}'.format(self.data.get('name'), self.data.get('newName')))
            if self.data.get('newName'):
                if self.containerExists(self.data.get('newName')):
                    raise ValueError('Container with that name already exists')
            container = self.client.instances.get(self.data.get('name'))
            previousState = container.status
            if previousState == 'Running':
                if force == False:
                    raise ValueError('Instance is running')
                container.stop(wait=True)
            container.rename(self.data.get('newName'), True)
            if previousState == 'Running':
                container.start(wait=True)
            self.data['name'] = self.data.get('newName')
            return self.info()
        except Exception as e:
            logging.error('Failed to rename instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)


    def freeze(self, waitIt=True):
        try:
            logging.info('Freezing instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            container.freeze(wait=waitIt)
        except Exception as e:
            logging.error('Failed to freeze instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)


    def unfreeze(self, waitIt=True):
        try:
            logging.info('Unfreezing instance {}'.format(self.data.get('name')))
            container = self.client.instances.get(self.data.get('name'))
            container.unfreeze(wait=waitIt)
        except Exception as e:
            logging.error('Failed to unfreeze instance {}'.format(self.data.get('name')))
            logging.exception(e)
            raise ValueError(e)

    def initNetwork(self):
        if not self.data.get('devices', None):
            self.data['devices']={}

    def addNetwork(self, network):
        self.initNetwork()
        self.data['devices'][network['name']]=network
        try:
            container = self.client.instances.get(self.data['name'])
            container.devices = self.data['devices']
            container.save()
            return self.info()
        except Exception as e:
            raise ValueError(e)

    def removeNetwork(self, networkName):
        self.initNetwork()
        del self.data['devices'][networkName]
        try:
            container = self.client.instances.get(self.data['name'])
            container.devices = self.data['devices']
            container.save()
            return self.info()
        except Exception as e:
            raise ValueError(e)

    def addProxy(self, name, proxy):
        self.initNetwork()
        self.data['devices'][name] = proxy
        try:
            container = self.client.instances.get(self.data['name'])
            container.devices = self.data['devices']
            container.save()
            return self.info()
        except Exception as e:
            raise ValueError(e)

    def removeProxy(self, name):
        self.initNetwork()
        del self.data['devices'][name]
        try:
            container = self.client.instances.get(self.data['name'])
            container.devices = self.data['devices']
            container.save()
            return self.info()
        except Exception as e:
            raise ValueError(e)
    
# The instance name character rules are listed on the official LXD documentation below:
# https://lxd.readthedocs.io/en/latest/instances/#properties
def verifyName(name):
    nameLength = len(name)
    if nameLength < 1 or nameLength > 63:
        logging.error('Instance name should be between 1 and 63 characters')
        raise ValueError('Instance name should be between 1 and 63 characters.')
 
    if name[0] == '-' or re.match('[0-9]', name[0]):
        logging.error('Instance name must start with a letter')
        raise ValueError('Instance name must start with a letter.')
    
    valid = re.match('^[a-zA-Z]+[a-zA-Z0-9-]*[a-zA-Z0-9]$', name)

    if not valid:
        logging.error('Instance name should only contain alphanumerics and dash')
        raise ValueError('Instance name should only contain alphanumerics and dash.')

def extractConfig(config):
    config_options = ['boot.autostart', 'boot.autostart.delay', 'boot.autostart.priority', 'boot.host_shutdown_timeout', 'boot.stop.priority', 'environment.*', 'limits.cpu', 'limits.cpu.allowance', 'limits.cpu.priority', 'limits.disk.priority', 'limits.kernel.*', 'limits.memory', 'limits.memory.enforce', 'limits.memory.swap', 'limits.memory.swap.priority', 'limits.network.priority', 'limits.processes','linux.kernel_modules', 'migration.incremental.memory', 'migration.incremental.memory.goal', 'migration.incremental.memory.iterations', 'nvidia.runtime', 'raw.apparmor', 'raw.idmap', 'raw.lxc', 'raw.seccomp', 'security.devkxd', 'security.devlxd.images', 'security.idmap.base','security.idmap.isolated', 'security.idmap.size', 'security.nesting', 'security.privileged', 'security.secureboot', 'security.syscalls.blacklist', 'security.syscalls.blacklist_compat', 'security.syscalls.blacklist_default', 'security.syscalls.whitelist']

    # remove all instance specific config options
    newInstanceConfig = config
    for option in list(newInstanceConfig):
        if option not in config_options:
            newInstanceConfig.pop(option, None)

    return newInstanceConfig
