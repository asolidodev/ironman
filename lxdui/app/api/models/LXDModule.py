from app.api.models.Base import Base
from app.api.utils.remoteImageMapper import remoteImagesList
from app.lib.conf import Config
from app import __metadata__ as meta

from pylxd import Client
import requests
import logging
import urllib3
import sys
import os

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logging = logging.getLogger(__name__)

class LXDModule(Base):
    # Default 127.0.0.1 -> Move to Config
    def __init__(self, remoteHost='127.0.0.1'):

        conf = Config()
        logging.info('Accessing PyLXD client')
        try:
            remoteHost = Config().get(meta.APP_NAME, '{}.lxd.remote'.format(meta.APP_NAME.lower()))
            sslKey = conf.get(meta.APP_NAME, '{}.ssl.key'.format(meta.APP_NAME.lower()))
            sslCert = conf.get(meta.APP_NAME, '{}.ssl.cert'.format(meta.APP_NAME.lower()))
            sslVerify = conf.get(meta.APP_NAME, '{}.lxd.sslverify'.format(meta.APP_NAME.lower()))

            if sslVerify.lower in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly']:
                sslVerify = True
                self.client = Client(endpoint=remoteHost,
                    cert=(sslCert, sslKey), verify=sslVerify)
            else:
                self.client = Client(endpoint=remoteHost, cert=(sslCert, sslKey),verify=False)
        except:
            logging.info('using local socket')
            self.client = Client()

    def listContainers(self):
        try:
            logging.info('Reading container list')
            return self.client.instances.all()
        except Exception as e:
            logging.error('Failed to read container list: ')
            logging.exception(e)
            raise ValueError(e)

    def listLocalImages(self):
        try:
            logging.info('Reading local image list')
            results = []
            for image in self.client.api.images.get().json()['metadata']:
                results.append(self.client.api.images[image.split('/')[-1]].get().json()['metadata'])

            return results
        except Exception as e:
            logging.error('Failed to read local image list: ')
            logging.exception(e)
            raise ValueError(e)

    def listRemoteImages(self):
        try:
            remoteImagesLink = Config().get(meta.APP_NAME, '{}.images.remote'.format(meta.APP_NAME.lower()))
            logging.info('Reading remote image list')
            remoteClient = Client(endpoint=remoteImagesLink)
            return remoteImagesList(remoteClient.api.images.aliases.get().json())
        except Exception as e:
            logging.error('Failed to get remote container images: ')
            logging.exception(e)
            raise ValueError(e)

    def listNightlyImages(self):
        try:
            logging.info('Reading nightly remote image list')
            images = requests.get(url='https://vhajdari.github.io/lxd-images/images.json')
            return images.json()['images']
        except Exception as e:
            logging.error('Failed to get remote nightly container images: ')
            logging.exception(e)
            raise ValueError(e)

    def listHubImages(self):
        try:
            logging.info('Listing images')
            result = requests.get('{}/cliListRepos'.format(meta.IMAGE_HUB))

            return result.json()
        except Exception as e:
            logging.error('Failed to list images from kuti.io')
            logging.exception(e)
            raise ValueError(e)

    def detailsRemoteImage(self, alias):
        try:
            remoteImagesLink = Config().get(meta.APP_NAME, '{}.images.remote'.format(meta.APP_NAME.lower()))
            remoteClient = Client(endpoint=remoteImagesLink)
            fingerprint = remoteClient.api.images.aliases[alias].get().json()['metadata']['target']
            return remoteClient.api.images[fingerprint].get().json()['metadata']
        except Exception as e:
            raise ValueError(e)

    def downloadImage(self, image):
        try:
            remoteImagesLink = Config().get(meta.APP_NAME, '{}.images.remote'.format(meta.APP_NAME.lower()))
            logging.info('Downloading remote image:', image)
            remoteClient = Client(endpoint=remoteImagesLink)
            try:
                remoteImage = remoteClient.images.get_by_alias(image)
            except:
                remoteImage = remoteClient.images.get(image)
            newImage = remoteImage.copy(self.client, auto_update=False, public=False, wait=True)
            return self.client.api.images[newImage.fingerprint].get().json()['metadata']
        except Exception as e:
            logging.error('Failed to download image:')
            logging.exception(e)
            raise ValueError(e)

    def deleteImage(self):
        pass

    def listProfiles(self):
        try:
            results = []
            for profile in self.client.profiles.all():
                profile_data = {}
                profile_data['config'] = profile.config
                profile_data['description'] = profile.description
                profile_data['devices'] = profile.devices
                profile_data['name'] = profile.name
                profile_data['used_by'] = profile.used_by
                results.append(profile_data)
 
            return results
        except Exception as e:
            raise ValueError(e)

    # this function returns the profile name options only as
    # other parameters significantly increase execution time
    def listMinimalProfiles(self):
        try:
            results = []
            for profile in self.client.profiles.all():
                profile_data = {}
                profile_data['name'] = profile.name
                results.append(profile_data)

            return results
        except Exception as e:
            raise ValueError(e)


    def listStoragePools(self):
        try:
            results = []
            for storage_pool in self.client.storage_pools.all():
                storage_data = {}
                storage_data['name'] = storage_pool.name
                storage_data['driver'] = storage_pool.driver
                storage_data['used_by'] = storage_pool.used_by
                storage_data['config'] = storage_pool.config
                storage_data['managed'] = storage_pool.managed
                results.append(storage_data)

            return results
        except Exception as e:
            raise ValueError(e)

    def listClusterHostnames(self):
        try:
            results = []
            # disables writing to stdout on this API call to keep logs clean
            sys.stdout = open(os.devnull, 'w')
            for host in self.client.cluster.get().members.all():
                if host.server_name != 'none':
                    results.append(host.server_name)
            # re-enable stdout output
            sys.stdout = sys.__stdout__
            return results
        except:
            return []

    def createProfile(self):
        pass

    def deleteProfile(self):
        pass

    def updateProfile(self):
        pass

    def listNetworks(self):
        try:
            logging.info('Retrieving network list.')
            results = []
            for network in self.client.networks.all():
                network_data = {}
                network_data['name'] = network.name
                network_data['description'] = network.description
                network_data['type'] = network.type
                network_data['used_by'] = network.used_by
                network_data['config'] = network.config
                network_data['managed'] = network.managed
                results.append(network_data)
            
            return results
        except Exception as e:
            logging.error('Failed to retrieve network list:')
            logging.exception(e)
            raise ValueError(e)

    def createNetwork(self):
        pass

    def deleteNetwork(self):
        pass

    def updateNetwork(self):
        pass


    def config(self):
        try:
            return self.client.api.get().json()['metadata']
        except Exception as e:
            raise ValueError(e)

    def hasImage(self, imageInfo):
        lxdModule = LXDModule()
        for image in lxdModule.listLocalImages():
            if image.get('fingerprint') == imageInfo:
                return 'fingerprint'
            for alias in image.get('aliases'):
                if alias.get('name') == imageInfo:
                    return 'alias'
        return None

    def containerExists(self, containerName):
        lxdModule = LXDModule()
        try:
            logging.info('Checking if container exists.')
            container = self.client.instances.get(containerName)
            return True
        except Exception as e:
            logging.error('Failed to verify container:')
            logging.exception(e)
            return False

    def info(self):
        raise NotImplementedError()

    def create(self):
        raise NotImplementedError()

    def delete(self):
        raise NotImplementedError()

    def start(self):
        raise NotImplementedError()

    def stop(self):
        raise NotImplementedError()

    def restart(self):
        raise NotImplementedError()

    def update(self):
        raise NotImplementedError()

    def move(self):
        raise NotImplementedError()

    def clone(self):
        raise NotImplementedError()

    def snapshot(self):
        raise NotImplementedError()
